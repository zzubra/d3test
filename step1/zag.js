    function wrap2(text, width, xposition, txtWidth) {
      txtWidth.length = 0;
      text.each(function(d,i) {
        var text = d3.select(this),
            words = text.text().split(/[ ]|\n/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            ln = 0;
            sup_flag = false;

        // console.log(text);

        var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
        var tmpMax = 0;
        while (word = words.pop()) {

            if(word.indexOf("<s>")>=0){

                // console.log(word, word.indexOf("<s>"), word.indexOf("</s>"));

                var start = word.indexOf("<s>"),
                stop = word.indexOf("</s>"),
                string1 = word.substr(0,start),
                num = word.substr(start+3, stop-start-3),
                string2 = word.substr(stop+4);
                // var ln = (tmpMax>0)?++lineNumber:lineNumber;

                // console.log(string1,num,string2, string1.length);
                word = string1 + num + string2;
                console.log(word);

                sup_flag = true;

            }


          line.push(word);
          tspan.text(line.join(" "));
          console.log("TSPAN:     ",tspan.text());

          if (tspan.node().getComputedTextLength() > width) {

            // console.log("LINE:    ",line);
            line.pop();
            tspan.text(line.join(" "));
            // console.log("TSPAN new:     ",tspan.text());
            line = [word];
            // console.log(line);

            // if(word.indexOf("<s>")>=0){

            //     // console.log(word, word.indexOf("<s>"), word.indexOf("</s>"));

            //     var start = word.indexOf("<s>"),
            //     stop = word.indexOf("</s>"),
            //     string1 = word.substr(0,start),
            //     num = word.substr(start+3, stop-start-3),
            //     string2 = word.substr(stop+4),
            //     ln = (tmpMax>0)?++lineNumber:lineNumber;

            //     console.log(string1,num,string2);
            //     tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text(string1)
            //     .append("tspan").classed("chart-sup",true).attr("x", xposition).attr("y", y).attr("dy", ( ln * lineHeight + dy -10 + "em")).text(num)
            //     .append("tspan").attr("x", xposition).attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text("sAAAAAAAAA");
            // }
            // else


            // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
            // that's why ((tmpMax>0)?++lineNumber:lineNumber)

            ln = (tmpMax>0)?++lineNumber:lineNumber;

            if (!sup_flag) 
            tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word)
            else {
                console.log("++++", word, line);
                string2 = string2 + " ";
                console.log("!!!!!!!!!!!!!!!!", word,string1,num,string2);
                tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text(string1);
                tspan = text.append("tspan").classed("chart-sup",true).attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text(num);
                tspan = text.append("tspan").attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text(string2);
                tspan = text.append("tspan").attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text("");
                // .append("tspan").attr("x", xposition).attr("y", y).attr("dy", ( ln * lineHeight + dy + "em")).text(string2);
                line.pop();

            }

          }
          tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());
          sup_flag = false;

        }
        txtWidth.push(tmpMax);


        // console.log(tmpMax);
      });
    }


    function textstyle(dd){

        dd.each(function(d,i) {
            var text = d3.select(this),
                //replace <*> in data texts to blue / slash
                newtext = text.html().replace(/\&lt\;\*\&gt\;/,'</tspan><tspan class="chart-blue">/</tspan><tspan>');
            text.html(newtext);
            // console.log(text);
            // words = text.text().split(/\*/),
            // ht = "";

            // console.log(tt);

            // if(words.length>1) {
            //     console.log(text.html());
            //     // ht = text.html();
            //     ht = text.html().match(/\<tspan.*?\>(.*?)\<\/tspan\>/g);
            //     console.log(ht);
            //     ht.forEach( function(x) {
            //         console.log(x.match(/\<tspan.*?\>(.*?)\<\/tspan\>/)[1]);
            //     })
            //     // console.log(ht.length,ht[0],ht[1]);



            // }

        })
    }