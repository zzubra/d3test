/**
*
* Includes: globals etc
*
**/



/*===============================
=            Globals            =
===============================*/

var base_place = "body";

var fullwidth = 538,
    barHeight = 18,
    barZoom = 5,
    barHeightBig = barHeight+barZoom,
    skos = barHeight/2.2,
    bigskos = barHeightBig/2.2,  
    // placefortext = 30,
    // line_width = 50,
    mtop = 50,
    mbottom = 50,
    // dist2line = 30,
    // start_text = 50,
    // bar_width = fullwidth - start_text - dist2line - line_width - placefortext,
    dot_size = 2,
    gap_between_bars = 0,
    gap_between_groups = 10,
    gap_to_legend = 30;

var h_legend_text = 10;

var g_duration = 1000; 

var w_group_name = 30,
    w_line = 50,
    w_value = 5,  // base width of value field
    w_value_one_character = 5,  // width of one character in value field 
    //total width of field = w_value + w_value_one_character * d.val length
    w_gap = 10,
    w_bar = fullwidth - ( w_group_name + w_gap + w_gap + w_line + w_gap + w_value),
    w_bar_start_width = 10,   // minimum width of bar to start growth animation
    w_legend_bar = 22;

var fadeLevel = 0.2, // level of fading of charts
    Time2KeepCategoryActivated = 5000;  // 4 sec keep legent category activated after click and activate all after that


/*-----  End of Globals  ------*/




/*=================================
=            Functions            =
=================================*/


function randomData() {
    return Math.random() * 9;
}


// var barFun = function(x) { 
//     return ["M",[skos,0],
//     "L",[x,0],
//     "L",[x,skos],
//     "L",[x-skos,barHeight],
//     "L",[0,barHeight],
//     "L",[0,barHeight-skos],
//     "L",[skos,0],       
//     "Z"].join(" ");
//     }

var barFun = function(x) { 
    return ["m",[skos,0],
    "l",[x-skos,0],
    "l",[0,skos],
    "l",[-skos,barHeight-skos],
    "l",[-x+skos,0],
    "l",[0,-(barHeight-skos)],
    "l",[skos,-skos],       
    "Z"].join(" ");
    }

var barFunLeft = function(x) { 
    return ["m",[skos,0],
    "l",[0,0],
    "l",[0,skos],
    "l",[-skos,barHeight-skos],
    "l",[0,0],
    "l",[0,-(barHeight-skos)],
    "l",[skos,-skos],       
    "Z"].join(" ");
    }


function ColorLuminance(hex, lum) {

    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    var rgb = "#", c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00"+c).substr(c.length);
    }

    return rgb;
}


/*-----  End of Functions  ------*/





