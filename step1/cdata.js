/**
*
* Chart data 
*
**/

 var c01_01 = [
 { "group_name":"2010",
     "group": [
     {"val": 349, "category": "Стратегия"},
     {"val": 349, "category": "Факт"}
     ]
 },

 { "group_name":"2011",
     "group": [
     {"val": 454, "category": "Стратегия"},
     {"val": 505, "category": "Факт"}
     ]
 },

 { "group_name":"2012",
     "group": [
     {"val": 531, "category": "Стратегия"},
     {"val": 720, "category": "Факт"}
     ]
 },

 {"group_name":"2013",
     "group": [
     {"val": 622, "category": "Стратегия"},
     {"val": 975, "category": "Факт"}
     ]
 },

 { "group_name":"2014",
     "group": [
     {"val": 727, "category": "Стратегия"},
     ]
 },
 { "group_name":"2015",
     "group": [
     {"val": 850, "category": "Стратегия"},
     ]
 }
 ];

var data2 = [
    
    { "group_name":"2010",
        "group": [
        {"val": 306.9, "category": "Strategy", "name": ""},
        {"val": 306, "category": "Актуальный результат прошлого года", "name": ""}
        ]
    },

    { "group_name":"2011",
        "group": [
        { "val": 416, "category": "Strategy", "name": ""},
        { "val": 459, "category": "Актуальный результат прошлого года", "name": ""}
        ]
    },

    { "group_name":"2012",
        "group": [
        {"val": 51.319, "category": "Strategy", "name": ""},
        {"val": 512, "category": "Актуальный результат прошлого года", "name": ""},
        {"val": 512, "category": "New Category", "name": ""}
        ]
    },


    {"group_name":"2013",
        "group": [
        {"val": 669, "category": "Актуальный результат прошлого года", "name": ""}
        ]
    },

    { "group_name":"2014",
        "group": [
        {"val": 51.319, "category": "Значение запланированное стратегией на 2013 год", "name": ""},
        {"val": 512, "category": "Актуальный результат прошлого года", "name": ""},
        {"val": 512, "category": "New Category", "name": ""}
        ]
    }
];

var data2c = {"Strategy":"#999999",  "Актуальный результат прошлого года":"#21a7d6",  "New Category":"#cdcdce", "Значение запланированное стратегией на 2013 год":"#78bfe2"};



var data3 = [
    

    { "group_name":"2012",
        "group": [
        {"val": 51.319, "category": "Strategy", "name": ""},
        {"val": 512, "category": "Актуальный результат прошлого года", "name": ""},
        {"val": 512, "category": "New Category", "name": ""}
        ]
    },


    {"group_name":"2013",
        "group": [
        {"val": 669, "category": "Актуальный результат прошлого года", "name": ""}
        ]
    },

    { "group_name":"2014",
        "group": [
        {"val": 51.319, "category": "Значение запланированное стратегией на 2013 год", "name": ""},
        {"val": 512, "category": "Актуальный результат прошлого года", "name": ""},
        {"val": 512, "category": "New Category", "name": ""}
        ]
    }
];

var data3c = {"Strategy":"#999999",  "Актуальный результат прошлого года":"#21a7d6",  "New Category":"#cdcdce", "Значение запланированное стратегией на 2013 год":"#78bfe2"};







