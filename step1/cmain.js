/*===============================
=            Globals            =
===============================*/

/**
*
* Chart data 
*
**/

var cdata_colors1 = ["#999999","#21a7d6","#cdcdce","#78bfe2"];


//  \n  первод строки
// <*>  синий слеш для буллетов
// <ЦИФРЫ>  синий суперскрипт
// \xA0  неразрывный
// legend_align: "left" - по умолчанию,"right","justify"


var c01_01_data = {
    title: "Объем кредитного портфеля банка развития, млрд рублей",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 349, "category": "Стратегия"},
            {"val": 349, "category": "Факт"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 454, "category": "Стратегия"},
            {"val": 505, "category": "Факт"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 531, "category": "Стратегия"},
            {"val": 720, "category": "Факт"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 622, "category": "Стратегия"},
            {"val": 975, "category": "Факт"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 727, "category": "Стратегия"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 850, "category": "Стратегия"},
            ]
        }
    ]
};
var en_c01_01_data = {
    title: "Development Bank’s Loan Portfolio, RUB bn",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 349, "category": "Strategy"},
            {"val": 349, "category": "Actual result"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 454, "category": "Strategy"},
            {"val": 505, "category": "Actual result"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 531, "category": "Strategy"},
            {"val": 720, "category": "Actual result"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 622, "category": "Strategy"},
            {"val": 975, "category": "Actual result"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 727, "category": "Strategy"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 850, "category": "Strategy"},
            ]
        }
    ]
};



var c01_03_data = {
    title: "Объем кредитов, предоставляемых в целях реализации инвестиционных проектов, млрд рублей",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 306, "category": "Стратегия"},
            {"val": 306, "category": "Факт"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 416, "category": "Стратегия"},
            {"val": 459, "category": "Факт"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 512, "category": "Стратегия"},
            {"val": 669, "category": "Факт"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 598, "category": "Стратегия"},
            {"val": 893, "category": "Факт"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 700, "category": "Стратегия"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 822, "category": "Стратегия"},
            ]
        }
    ]
};
var en_c01_03_data = {
    title: "Loans Extended for Investment Projects, RUB bn",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 306, "category": "Strategy"},
            {"val": 306, "category": "Actual result"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 416, "category": "Strategy"},
            {"val": 459, "category": "Actual result"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 512, "category": "Strategy"},
            {"val": 669, "category": "Actual result"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 598, "category": "Strategy"},
            {"val": 893, "category": "Actual result"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 700, "category": "Strategy"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 822, "category": "Strategy"},
            ]
        }
    ]
};


var c01_04_data = {
    title: "Динамика основных финансовых показателей, млрд рублей",
    w_group_name: 160,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#78bfe2","#21a7d6"],
    data: [
        { "group_name":"Валюта баланса<2>",
            "group": [
            {"val": 2296.5, "category": "01.01.2012"},
            {"val": 2588.9, "category": "01.01.2013"},
            {"val": 2966.3, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Собственные средства\n(капитал)",
            "group": [
            {"val": 336.8, "category": "01.01.2012"},
            {"val": 314.0, "category": "01.01.2013"},
            {"val": 335.4, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Прибыль",
            "group": [
            {"val": 19.79, "category": "01.01.2012"},
            {"val": 5.41, "category": "01.01.2013"},
            {"val": 20.58, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Ресурсы, привлеченные на\xA0срочной основе от\xA0банков",
            "group": [
            {"val": 314.5, "category": "01.01.2012"},
            {"val": 386.5, "category": "01.01.2013"},
            {"val": 443.9, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Ресурсы, привлеченные в\xA0результате размещения облигаций",
            "group": [
            {"val": 174.6, "category": "01.01.2012"},
            {"val": 271.2, "category": "01.01.2013"},
            {"val": 470.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Вложения инвестиционного характера в\xA0акции (доли в\xA0уставном капитале) организаций",
            "group": [
            {"val": 232.6, "category": "01.01.2012"},
            {"val": 294.4, "category": "01.01.2013"},
            {"val": 338.5, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Кредитный портфель Внешэкономбанка, в\xA0том числе:",
            "group": [
            {"val": 766.6, "category": "01.01.2012"},
            {"val": 1002.3, "category": "01.01.2013"},
            {"val": 1318.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0кредитный портфель \xA0\xA0\xA0банка\xA0развития<3>",
            "group": [
            {"val": 505.4, "category": "01.01.2012"},
            {"val": 720.2, "category": "01.01.2013"},
            {"val": 974.6, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0прочие кредиты",
            "group": [
            {"val": 261.2, "category": "01.01.2012"},
            {"val": 282.1, "category": "01.01.2013"},
            {"val": 344.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Портфель ценных бумаг<4>",
            "group": [
            {"val": 539.2, "category": "01.01.2012"},
            {"val": 517.8, "category": "01.01.2013"},
            {"val": 508.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Портфель гарантий и\xA0поручительств",
            "group": [
            {"val": 132.7, "category": "01.01.2012"},
            {"val": 133.0, "category": "01.01.2013"},
            {"val": 219.2, "category": "01.01.2014"}
            ]
        }
    ]
};
var en_c01_04_data = {
    title: "Development Dynamics: Major Financial Highlights, RUB bn",
    w_group_name: 160,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#78bfe2","#21a7d6"],
    data: [
        { "group_name":"Total assets<2>",
            "group": [
            {"val": 2296.5, "category": "01.01.2012"},
            {"val": 2588.9, "category": "01.01.2013"},
            {"val": 2966.3, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Capital",
            "group": [
            {"val": 336.8, "category": "01.01.2012"},
            {"val": 314.0, "category": "01.01.2013"},
            {"val": 335.4, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Income",
            "group": [
            {"val": 19.79, "category": "01.01.2012"},
            {"val": 5.41, "category": "01.01.2013"},
            {"val": 20.58, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Term resources raised from banks",
            "group": [
            {"val": 314.5, "category": "01.01.2012"},
            {"val": 386.5, "category": "01.01.2013"},
            {"val": 443.9, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Resources raised from bond placement",
            "group": [
            {"val": 174.6, "category": "01.01.2012"},
            {"val": 271.2, "category": "01.01.2013"},
            {"val": 470.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Equity investment in authorized capital of\xA0companies",
            "group": [
            {"val": 232.6, "category": "01.01.2012"},
            {"val": 294.4, "category": "01.01.2013"},
            {"val": 338.5, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Loan portfolio of Vnesh­econom­bank, including:",
            "group": [
            {"val": 766.6, "category": "01.01.2012"},
            {"val": 1002.3, "category": "01.01.2013"},
            {"val": 1318.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0Loan portfolio of the Bank \xA0\xA0\xA0for\xA0Development<3>",
            "group": [
            {"val": 505.4, "category": "01.01.2012"},
            {"val": 720.2, "category": "01.01.2013"},
            {"val": 974.6, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0Other loans",
            "group": [
            {"val": 261.2, "category": "01.01.2012"},
            {"val": 282.1, "category": "01.01.2013"},
            {"val": 344.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Securities portfolio<4>",
            "group": [
            {"val": 539.2, "category": "01.01.2012"},
            {"val": 517.8, "category": "01.01.2013"},
            {"val": 508.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Portfolio of guarantees and sureties",
            "group": [
            {"val": 132.7, "category": "01.01.2012"},
            {"val": 133.0, "category": "01.01.2013"},
            {"val": 219.2, "category": "01.01.2014"}
            ]
        }
    ]
};


var c02_03_data = {
    title: "Портфель «инвестиционных» кредитов (в разрезе основных целей их реализации), млрд рублей",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Развитие инфраструктуры",
            "group": [
            {"val": 385.9, "category": "01.01.2014"},
            {"val": 267.8, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Создание производства",
            "group": [
            {"val": 308.1, "category": "01.01.2014"},
            {"val": 230.0, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Развитие производства",
            "group": [
            {"val": 143.4, "category": "01.01.2014"},
            {"val": 121.9, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Корпоративное финансирование",
            "group": [
            {"val": 38.5, "category": "01.01.2014"},
            {"val": 35.5, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Иные цели",
            "group": [
            {"val": 17.1, "category": "01.01.2014"},
            {"val": 13.7, "category": "01.01.2013"}
            ]
        }

    ]
};
var en_c02_03_data = {
    title: "Portfolio of investment loans (by projects categorized by delivery goals), RUB bn",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Development of infrastructure",
            "group": [
            {"val": 385.9, "category": "01.01.2014"},
            {"val": 267.8, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Construction of production facilities",
            "group": [
            {"val": 308.1, "category": "01.01.2014"},
            {"val": 230.0, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Development of production facilities",
            "group": [
            {"val": 143.4, "category": "01.01.2014"},
            {"val": 121.9, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Corporate finance",
            "group": [
            {"val": 38.5, "category": "01.01.2014"},
            {"val": 35.5, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Other goals",
            "group": [
            {"val": 17.1, "category": "01.01.2014"},
            {"val": 13.7, "category": "01.01.2013"}
            ]
        }

    ]
};


var c02_06_data = {
    title: "Динамика показателей деятельности по поддержке экспорта, млрд рублей",
    w_group_name: 60,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#78bfe2","#999999"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 47.93, "category": "Гарантии в целях поддержки экспорта"},
            {"val": 12.07, "category": "Предэкспортное финансирование"},
            {"val": 2.57, "category": "Экспортные кредиты"}
            ]
        },
        { "group_name":"01.01.2014",
            "group": [
            {"val": 99.57, "category": "Гарантии в целях поддержки экспорта"},
            {"val": 6.88, "category": "Предэкспортное финансирование"},
            {"val": 15.65, "category": "Экспортные кредиты"}
            ]
        }
    ]
};
var en_c02_06_data = {
    title: "Development dynamics: export support activities (RUB, bn)",
    w_group_name: 60,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#78bfe2","#999999"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 47.93, "category": "Guarantees to support exports"},
            {"val": 12.07, "category": "Pre-export finance"},
            {"val": 2.57, "category": "Export credits"}
            ]
        },
        { "group_name":"01.01.2014",
            "group": [
            {"val": 99.57, "category": "Guarantees to support exports"},
            {"val": 6.88, "category": "Pre-export finance"},
            {"val": 15.65, "category": "Export credits"}
            ]
        }
    ]
};


var c02_07_data = {
    title: "Структура средств, привлеченных на рынках капитала, в разрезе срочности, млрд рублей",
    w_group_name: 110,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Свыше 5\xA0лет",
            "group": [
            {"val": 454.3, "category": "01.01.2014"},
            {"val": 400.6, "category": "01.01.2013"}
            ]
        },
        { "group_name":"От 3 до\xA05\xA0лет",
            "group": [
            {"val": 236.4, "category": "01.01.2014"},
            {"val": 24.8, "category": "01.01.2013"}
            ]
        },
        { "group_name":"От 1 года до\xA03\xA0лет",
            "group": [
            {"val": 122.7, "category": "01.01.2014"},
            {"val": 102.4, "category": "01.01.2013"}
            ]
        }
    ]
};
var en_c02_07_data = {
    title: "Structure of funds borrowed in capital markets (by maturity), RUB bn",
    w_group_name: 110,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Over 5 years",
            "group": [
            {"val": 454.3, "category": "01.01.2014"},
            {"val": 400.6, "category": "01.01.2013"}
            ]
        },
        { "group_name":"3 to 5 years",
            "group": [
            {"val": 236.4, "category": "01.01.2014"},
            {"val": 24.8, "category": "01.01.2013"}
            ]
        },
        { "group_name":"1 to 3 years",
            "group": [
            {"val": 122.7, "category": "01.01.2014"},
            {"val": 102.4, "category": "01.01.2013"}
            ]
        }
    ]
};


var c03_01_data = {
    title: "Оборот по операциям привлечения/размещения краткосрочных МБК, млрд рублей (эквивалент по среднему курсу)",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"Привлеченные средства",
            "group": [
            {"val": 5491, "category": "2012"},
            {"val": 5056, "category": "2013"}
            ]
        },

        { "group_name":"Размещенные средства",
            "group": [
            {"val": 7385, "category": "2012"},
            {"val": 7576, "category": "2013"}
            ]
        }
    ]
};
var en_c03_01_data = {
    title: "Turnover of money market operations (short-term interbank loans), RUB bn (in equivalent to an average yearly exchange rate)",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"Funds raised",
            "group": [
            {"val": 5491, "category": "2012"},
            {"val": 5056, "category": "2013"}
            ]
        },

        { "group_name":"Funds placed",
            "group": [
            {"val": 7385, "category": "2012"},
            {"val": 7576, "category": "2013"}
            ]
        }
    ]
};


/*=================================
=          Global Functions       =
=================================*/


function randomData() {
    return Math.random() * 9;
}


// var barFun = function(x) { 
//     return ["M",[skos,0],
//     "L",[x,0],
//     "L",[x,skos],
//     "L",[x-skos,barHeight],
//     "L",[0,barHeight],
//     "L",[0,barHeight-skos],
//     "L",[skos,0],       
//     "Z"].join(" ");
//     }




/*-----  End of Global Functions  ------*/



function barChart(place,cdata,w) {

    var uid = generateUID();

    var base_place = "body";

    var chart_fullwidth = 538,
        barHeight = 18,
        barZoom = 5,
        barHeightBig = barHeight+barZoom,
        skos = barHeight/2,
        // bigskos = barHeightBig/2.2,  
        // placefortext = 30,
        // line_width = 50,
        mtop = 50,
        mbottom = 50,
        // dist2line = 30,
        // start_text = 50,
        // bar_width = fullwidth - start_text - dist2line - line_width - placefortext,
        dot_size = 2,
        gap_between_bars = 0,
        gap_between_groups = 10,
        gap_to_legend = 30;

    var h_legend_text = 10;

    var g_duration = 1000; 

    var w_line = 50,
        w_value_one_character = 5,  // width of one character in value field 
        //total width of field = w_value + w_value_one_character * d.val length
        w_gap = 10,
        w_gap_between_legends = 3*w_gap,
        w_bar_start_width = 10,   // minimum width of bar to start growth animation
        w_legend_bar = 22;

    var fadeLevel = 0.2, // level of fading of charts
        Time2KeepCategoryActivated = 5000;  // 4 sec keep legent category activated after click and activate all after that

        var ru_RU = {
          "decimal": ",",
          "thousands": "\xa0",
          "grouping": [3],
          "currency": ["", " руб."],
          "dateTime": "%A, %e %B %Y г. %X",
          "date": "%d.%m.%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
          "shortDays": ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
          "months": ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
          "shortMonths": ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
        };
        var en_US ={
          "decimal": ".",
          "thousands": ",",
          "grouping": [3],
          "currency": ["$", ""],
          "dateTime": "%a %b %e %X %Y",
          "date": "%m/%d/%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        };

        var d3RU = d3.locale(ru_RU),
            d3EN = d3.locale(en_US),
            local_num_format = d3RU.numberFormat();
            // local_num_format = d3EN.numberFormat();

    /*-----  End of Globals  ------*/



    w = (typeof w === "undefined") ? chart_fullwidth : w;   // if no w set, default value is chart_fullwidth

    var _chart = {},

        _width = w, // to change width of chart dynamicaly we have to pass w to function
        _height = 300,  // full height chart+legend (start with free value 300px)
        // _margins = {top: 0, left: 0, right: 0, bottom: 0},
        _x, _y,
        _data = [],
        _data2colors = [],
        // _colors = d3.scale.category10(),
        _svg,
        _bodyG,

        _max_data_value = 0,

        _groups_total = 0,
        _bars_total = 0,
        _height_chart = 0,  // height of chart part without legend
        _legendSpace = 0,  // horizontal width of one legend block
        _legendHeight = barHeight,  // starting value for height of legend block
        _legendAlign = "left",

        _legendTextWidth = [],
        _group_names_TextWidth = [],

        _categoryNames = [],  // list of unique data.group.categories
        _categoryMap = [],  // list on category objects for chart legend: {category,active}

        _tooltip,
        _timerId,
        _w_group_name,
        _w_bar,
        _w_value = 5;  // base width of value field;

    var _tag_replace = /[|&;$%@"<>()+,. ]/g;  // regex to replace invalid characters in selector name




    var barFun = function(x) { 
        return ["m",[skos,0],
        "l",[x-skos,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[-x+skos,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],       
        "Z"].join(" ");
        }

    var barFunLeft = function(x) { 
        return ["m",[skos,0],
        "l",[0,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[0,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],       
        "Z"].join(" ");
        }

    function generateUID() {
        return ("0000" + (Math.random()*Math.pow(36,4) << 0).toString(36)).slice(-4)
    }

    function ColorLuminance(hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i*2,2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00"+c).substr(c.length);
        }

        return rgb;
    }


    function quadrantWidth() {
        // return _width - _margins.left - _margins.right;
        return _width;

    }

    function quadrantHeight() {
        // return _height - _margins.top - _margins.bottom;
        return _height;

    }


    //.setSeries – set data and prepare corresponding vars
    _chart.setSeries = function (series) {
        _data = series.data;
        // _data2colors = colors;
        _legendHeight = series.legendHeight;
        _legendAlign = (typeof series.legend_align === "undefined") ? "left" : series.legend_align;

        _w_group_name = series.w_group_name;
        _w_bar = _width - ( _w_group_name + w_gap + w_gap + w_line + w_gap + _w_value);


        _groups_total = _data.length;

        _max_data_value = 0;
        // calculate max value of ALL val
        _max_data_value = d3.max(_data, 
        function(d) {
            
            return d3.max(d.group, function(d) { 
                // var  prec = (d.val + "").split("."),
                //      round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;

                // console.log(d.val, (d.val + "").length, prec, round);
                return parseFloat(d.val); })
        });

        _bars_total = 0;
        _data.forEach(function(d,i) {           
            // calculate total number of bars
            // number of elements in d.group is a number of bars
            _bars_total += d.group.length;
        })


        /*==========  prepare legend  ==========*/

        // clear legend arrays
        _categoryNames.length = 0;
        _categoryMap.length = 0;

        //create list of unique data.group.categories 

        // data2.forEach(group.forEach( function(d) {categoryNames[d.category] = d;});
        _data.forEach( function(d){ 
            d.group.forEach( function(dd){_categoryNames.push(dd.category);} )  // collect all category names
        });
        _categoryNames = d3.set(_categoryNames).values();  // keep unique names only

        var tmpcolors = [];

        // for every unique category create object
        // to control if category of legend is active or not
        for (var ii = 0; ii < _categoryNames.length; ii++) {
            _categoryMap.push({
                category: _categoryNames[ii],
                active: true
            })

            // set colors for each category
            // set RED color for all categories with no color specified
            _data2colors[_categoryNames[ii]] = (ii<series.colors.length) ? series.colors[ii] : "red"; 
            
        }

        _legendSpace = _width/_categoryMap.length;
        // console.log(_legendSpace);



        /*==========  set heights  ==========*/

        // height of chart part
        _height_chart = barZoom + barHeight * _bars_total + gap_between_bars * (_bars_total - 1) + gap_between_groups * (_groups_total - 1);
        
        // total height of svg: height of chart + height of legend block
        _height = _height_chart + gap_to_legend + _legendHeight;  // add place for legend

        // console.log(_height_chart);
        _tooltipOn = series.tooltip;

        return _chart;
    };


    // .render – render chart
    _chart.render = function () {

        if (!_data.length) {console.log("ERROR: Set data with .setSeries before running .render"); return false;}
        if (!_svg) {  _svg = d3.select(base_place).select(place).append("svg");}
        if (!_tooltip) {  _tooltip = d3.select(base_place).select(place).append("div");}

        _svg
            // .transition().duration(200)
            .attr("height", _height)
            .attr("width", _width);

        _x = d3.scale.linear()
            .domain([0, _max_data_value])
            .range([w_bar_start_width, _w_bar]);


        // renderAxes(_svg);

        defineBodyClip(_svg);
        renderBody(_svg);
    };

    function defineBodyClip(svg) {
        var padding = 5,
            bodyclipplace = place+"_body-clip";

        svg.select("defs")
        .remove("defs");

        svg.append("defs")
                .append("clipPath")
                .attr("id", bodyclipplace)
                .append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", quadrantWidth() + 2 * padding)
                .attr("height", quadrantHeight());
    }

    function renderBody(svg) {
        var bodyclipplace = place+"_body-clip";
        if (!_bodyG)
            _bodyG = svg.append("g")
                    // .attr("class", "body")
                    // .attr("transform", "translate(" 
                    //         + xStart() 
                    //         + "," 
                    //         + yEnd() + ")")
                    .attr("clip-path", "url("+bodyclipplace+")");


        renderBars();
        renderLegend();
        renderTooltip();
    }
    
    function renderTooltip(){
        _tooltip 
            .classed("chart-tooltip",true)
            .style("visibility", "hidden");
    }

    function renderBars() {
        // var padding = 2; // <-A
        var tmp_height = barZoom;   // move first bar down for zoomBar to fit it zoomed version
        

        // add groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            .enter()
            .append("g")
            .attr('class', 'chart-groups');
        // update groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            // .transition()
            .attr("transform", function(d, i) { 
                var tmp_height2 = tmp_height;
                // console.log(d.group.length,tmp_height);
                tmp_height += d.group.length * barHeight + gap_between_bars * (d.group.length - 1) + gap_between_groups;
                return "translate(0," + tmp_height2 + ")"; });
            // console.log(_data[0].group);


        // add bars

        var barContainer = _bodyG.selectAll("g.chart-groups")    
            .selectAll("g.chart-bars")
            // .data(_data)
            .data(function(d){ return d.group})
            .enter()
            .append("g");


        barContainer
            .attr('class', 'chart-bars')
            .attr("transform", function(d, i) { return "translate(0," + (i * barHeight + gap_between_bars) +  ")"; })
            // .attr("id", 'tag'+d.category.replace(/\s+/g, ''));
            .attr("id", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })
        //draw bars with starting w_bar_start_width  
            .append("path")
            .attr('class', 'chart-paths')
            .attr({
                "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
                "fill": function(d) { return _data2colors[d.category]; },
                "d": function(d) { return barFun(_x(w_bar_start_width))}
            })
            .on("mouseover", function(d) {return onMouseOver_Bar(this)})
            .on("mousemove", function(d) {return onMouseMove_Bar(this)})
            .on("mouseout", function(d) {return onMouseOut_Bar(this)});

        // add values
         barContainer   
            .append("text")
                // .attr("x", function(d) { return x(d.val) + 50; })
                .attr("x", _width)
                .attr("y", barHeight / 2)
                .attr("dy", ".35em")
                .attr("text-anchor","end")
                .attr('class', 'chart-value_text')
                .text(function(d) { return 0; });

        // add dots on lines
         barContainer
             .append("rect")
                 .attr("transform", function(d) { return "translate(" + (_w_group_name + w_gap + _x(w_bar_start_width) + w_gap) + "," + (barHeight - dot_size)/2 + ")"; })
                 .attr("width", dot_size)
                 .attr("height", dot_size)
                 .style("fill","#999999");

        // add lines with starting length  according to starting w_bar_start_width 
         barContainer
             .append("line")
                // .transition().duration(2000) 
                .attr("x1", function(d) { return _w_group_name + w_gap + _x(w_bar_start_width) + w_gap;})
                .attr("y1", barHeight/2)
                .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)})
                .attr("y2", barHeight/2)
                .attr("stroke-width", 1)
                .attr("stroke", "#999999");


        // add group names  
        _bodyG.selectAll("text.chart-group_text")
            .data(_data)
            .enter()
            .append("text")
            .attr("x", 0)
            .attr("y", barHeight / 2)
            .attr("dy", ".35em")
            .attr('class', 'chart-group_text')
            .attr("transform", function(d, i) { 
                return "translate(0," + 0 + ")"; })
            .text(function(d) { return d.group_name});



        updateBars();
        

    }  // end of renderBars()


    function updateBars() {

        var bars =_bodyG.selectAll("g.chart-groups").selectAll("g.chart-bars")

        // update lines according to the _data width               
        bars
             // .data(_data)
             .data(function(d){ return d.group})
             .selectAll("line")
             .transition().duration(g_duration)
                .attr("x1", function(d) { return _w_group_name + w_gap +  _x(d.val) + w_gap;})

        // update dots on lines 
        bars
            // .data(_data)
            .data(function(d){ return d.group})
            .selectAll("rect")
                .transition().duration(g_duration)
                .attr("transform", function(d) { return "translate(" + (_w_group_name + w_gap + _x(d.val) + w_gap) + "," + (barHeight - dot_size)/2 + ")"; });

        // update bars to the d.val width 
        _bodyG.selectAll("g.chart-groups")
            .selectAll("g.chart-bars path")
            // .data(_data)
            .data(function(d){ return d.group})   
            .transition().duration(g_duration)
            .attr({
                "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
                "fill": function(d) { return _data2colors[d.category]; },
                "d": function(d) { return barFun(_x(d.val))}
            });


        // update values  
        bars
            // .data(_data)
            .data(function(d){return d.group})
            .selectAll("text")
                .transition().duration(g_duration)
                .tween("text", function(d) {
                    // get current value as starting point for tween animation
                   var currentValue = +this.textContent;
                   var ii = d3.interpolate(currentValue, d.val),
                       prec = (d.val + "").split("."),
                       round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                    // console.log(d3.select(this).attr("class"));
                    // d3.select(this).attr("class","chart-value_text");
                    // console.log(d3.select(this).attr("class"));

                    // this.attr( 'class', 'chart-value_text' );
                   return function(t) {
                       // this.textContent = Math.round(ii(t));
                       // this.attr("class","chart-value_text");
                       this.textContent = local_num_format(Math.round(ii(t) * round) / round);
                       // d3.select(this).attr("font-size","10px");

                   };
                });




        // update group names  
        tmp_height=0;
        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)
            // .enter()
            // .transition().duration(g_duration/4)
            .attr("transform", function(d, i) { 
                var tmp_height2 = tmp_height;
                tmp_height += d.group.length * barHeight + gap_between_bars * (d.group.length - 1) + gap_between_groups;
                return "translate(0," + tmp_height2 + ")"; })
            .call(wrap, _w_group_name - w_gap, 0, _group_names_TextWidth);

        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)    
            .call(textstyle);

            // .text(function(d) { console.log(d); return d.group_name});
    }  // end of updateBars()


    function renderLegend() {
        // Legend  

            // add legend

            var legendContainer = _bodyG.selectAll("g.chart-legend")    
                .data(_categoryMap)
                // .data(function(d){ return d.group})
                .enter()
                .append("g")
                .attr("class", "chart-legend")
                .attr("id", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })



            _bodyG.selectAll("g.chart-legend")    
                .data(_categoryMap)
                .attr("visibility","hidden")  // turn legend off to make all calculations for word wrap
                .attr("transform", function(d,i) { return "translate(" +  (i*_legendSpace) + "," + (_height_chart + gap_to_legend) + ")"; })
                .on("click", function(d) {return onClick_Legend(d)});

            legendContainer    
                    .append("path")

            legendContainer    
                    .append("text")                                    
                    .attr("class", "chart-legend_text")


            _bodyG.selectAll("g.chart-legend path")    
                // .data(_categoryMap)
                .attr({
                    "fill": function(d) { return _data2colors[d.category]},
                    "d": function(d) { return barFun(w_legend_bar)}
                })
                


            _bodyG.selectAll("g.chart-legend text.chart-legend_text")    
                // .data(_categoryMap)
                // .append("text") 
                .attr("x", function(d,i) { return (w_legend_bar + w_gap); }) 
                .attr("y", h_legend_text)
                // .attr("dy", ".35em")
                .attr("dy", "0")
                .text( function(d){ return d.category;})
                // .transition().delay(2000)
                .call(wrap, _legendSpace - w_legend_bar - w_gap - w_gap_between_legends, w_legend_bar + w_gap, _legendTextWidth);
                // .on("click", function(d){
                //     newOpacity = d.active ? 0.5 : 1; 
                //     d3.selectAll("#tag"+d.category.replace(/\s+/g, ''))
                //         .transition().duration(100) 
                //         .style("opacity", newOpacity); 
                //     d.active = !d.active;
                //     });
            var tmp = _legendTextWidth.length;
            var w_add = (tmp > 1) ? ((_width - (tmp * (w_legend_bar + w_gap) + d3.sum(_legendTextWidth)) ) /(tmp - 1) - w_gap_between_legends) : 0;
            // w_add is correction of each legend width to fill all _width with legends
            // w_add = 0;
            var w_add2 = _width - d3.sum(_legendTextWidth) - (w_legend_bar + w_gap)*tmp - w_gap_between_legends * (tmp - 1);
            // w_add2 += 2;
            // console.log(w_add2);

            // _legendAlign base value is "left"
            if ( _legendAlign == "right" ) { w_add = 0; }
            if ( _legendAlign == "justify" ) { w_add2 = 0; }
            if ( _legendAlign == "left" ) { w_add = 0; w_add2 = 0; }



            _bodyG.selectAll("g.chart-legend")
                // .transition()
                .attr("transform", 
                    function(d,i) {
                        var tmp_w = 0;
                        for (var ii = 0; ii < i; ii++) {
                            tmp_w += w_legend_bar + w_gap +_legendTextWidth[ii] + w_gap_between_legends + w_add;
                        };
                        return "translate(" +  (w_add2 + tmp_w) + "," + (_height_chart + gap_to_legend) + ")"; 
                    })

            _bodyG.selectAll("g.chart-legend")    
                .attr("visibility","visible") // turn legend ON. All calculations for word wrap are completed.

    } // end of renderLegend()


    // function: wrap long lines
    // text - data array
    // width – max width of text
    // xposition – starting X position of text inside <tspan>
    // txtWidth – after function completion this array has width of each of calculated lines

    function wrap(text, width, xposition, txtWidth) {
      txtWidth.length = 0;
      text.each(function(d,i) {
        var text = d3.select(this),
            // words = text.text().split(/[ \n]/).reverse(),
            words = text.text().split(/ /).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.2, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            ln = 0;


        var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
        var tmpMax = 0;
        while (word = words.pop()) {
            // console.log("_"+word+"_");
          line.push(word);
          tspan.text(line.join(" "));
          // console.log("_"+word+"_","---------------'",tspan.text(),"'", tspan.text().length);       


          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];

            // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
            // that's why ((tmpMax>0)?++lineNumber:lineNumber)
            ln = (tmpMax>0)?++lineNumber:lineNumber;
            tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word);
          }
          // console.log(tspan.node().innerHTML);
          tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());

        }
        txtWidth.push(tmpMax);
        // console.log(txtWidth);
        //replace <\d+> in data texts to superscript
        doSuperscript(text);

      });
    }

    function doSuperscript(text){
        //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
        // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
        //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
        if (text.html()){
            //replace <\d+> in data texts to superscript
            if(RegExp(/\&lt\;(\d+)\&gt\;/).test(text.html())) {
                var num = text.html().match(/\&lt\;(\d+)\&gt\;/)[1],
                    newtext = text.html().replace(/\&lt\;(\d+)\&gt\;/,'</tspan><tspan class="chart-sup" dy="-3">'+num+'</tspan><tspan>');
                // console.log(newtext);
                text.html(newtext);
            }
        } else {console.log("!!! https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");}
    }

    function textstyle(dd){

        dd.each(function(d,i) {
            var text = d3.select(this);

            //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
            // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
            //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
            if (text.html()){
                //replace <*> in data texts to blue / slash
                var newtext = text.html().replace(/\&lt\;\*\&gt\;/,'</tspan><tspan class="chart-blue">/</tspan><tspan>');
                text.html(newtext);
            } else {
                console.log("!!!https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");
                    // !!!!!!!!! https://code.google.com/p/innersvg/   needed here everywhere except chrome
            }

        })
    }

    function onClick_Legend(d){

        clearTimeout(_timerId);

        // Determine if current line is visible

        var newOpacity = 1,
            check_active = d.active;

        if (check_active) {
            _categoryMap.forEach( function(dd){ check_active = check_active && dd.active;} )
            newOpacity = check_active ? fadeLevel : 1;

        } else {
            newOpacity = fadeLevel;
            check_active = true;
            d.active = true;

        }

        // Hide or show the elements based on the ID

        // set opacity of other to newOpacity
        var cur_category = d.category;
        _bodyG.selectAll("[id^='tag']")
            .filter( function(dd) {
                // console.log(d.category,"    ",this.id == "tag" + cur_category.replace(/\s+/g, '')); 
                return (this.id == "tag" + uid + cur_category.replace(_tag_replace, '')) ? false : true
            })
            .transition().duration(100) 
            .style("opacity", newOpacity);

        // set opacity of selected to 1
        _bodyG.selectAll("#tag" + uid + cur_category.replace(_tag_replace, ''))
            .transition().duration(100) 
            .style("opacity", 1); 

        // Update whether or not the elements are active
        var cur_active = d.active;
        _categoryMap.forEach( function(dd){ dd.active = !check_active;} )
        d.active = cur_active;
        // console.log(d);

        _timerId = setTimeout(function() {
            _bodyG.selectAll("[id^='tag']")
            .transition().duration(g_duration) 
            .style("opacity", 1);
            _categoryMap.forEach( function(dd){ dd.active = true;} )
        }, Time2KeepCategoryActivated);
    }

    function onMouseOver_Bar(d) {

        var pathContainer = d3.select(d.parentNode),
            cur_cat = pathContainer[0][0].__data__.category,
            cur_val = pathContainer[0][0].__data__.val,
            isactive = false;


        onMouseOver_Tolltip(cur_val);



        // var allSVG = _svg;
        // allSVG
        //     .selectAll(".chart-zoombar")
        //     // .transition()
        //     // .attr("transform", "translate(" + (_w_group_name + w_gap ) + "," + (0) + ")")
        //     .remove();
        // allSVG
        //     .selectAll(".chart-zoombar_shadow_dark")
        //     .remove();
        // allSVG
        //     .selectAll(".chart-zoombar_shadow")
        //     .remove();
        // allSVG
        //     .selectAll("text")
        //     .classed("chart-selected_value", false);



        _categoryMap.forEach( function(el) {
            if (el.category===cur_cat &&  el.active==true) isactive = true; 
        })

        // if this Legend category ic active (not faded)
        if (isactive) {

            // var thisBar = d3.select(d);
            // console.log(thisBar);

            // thisBar
            // .transition().duration(g_duration/4)
            // .attr({
            //     "fill": "red"
            // })
            // .attr({
            //     // "fill": function(d) { return _data2colors[d.category]},
            //     "d": function(d) { return barFun_big(_x(d.val))}
            // })

            // var barContainer = _bodyG.selectAll("g.chart-groups").selectAll("g.chart-groups");
            // barContainer



            pathContainer
                .select("line")
                .transition() 
                .attr("x2", function(d) { return _w_group_name + w_gap + _x(d.val) + w_gap;})
                .transition() 
                .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)});


            // add shadow body    
            pathContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .attr({
                    // "id": "chart-zoombar_shadow",
                    "class": "chart-zoombar_shadow"
                    // "fill": "#0086cd"
                })
                .attr({
                    "fill": function(d) { return ColorLuminance(_data2colors[d.category], -0.3)},
                    "transform": "translate(" + (_w_group_name + w_gap) + "," + (0) + ")",
                    // "fill": function(d) { return _data2colors[d.category]; },
                    // "d": function(d) { return barFun(_x(w_bar_start_width))}
                    "d": function(d) { return barFun(_x(d.val))}
                })

            // add dark left shadow part    
            pathContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .attr({
                    // "id": "chart-zoombar_shadow",
                    "class": "chart-zoombar_shadow_dark"
                    // "fill": "#0086cd"
                })
                .attr({
                    "fill": function(d) { return ColorLuminance(_data2colors[d.category], -0.5)},
                    "transform": "translate(" + (_w_group_name + w_gap) + "," + (0) + ")",
                    // "fill": function(d) { return _data2colors[d.category]; },
                    // "d": function(d) { return barFun(_x(w_bar_start_width))}
                    "d": function(d) { return barFunLeft(_x(d.val)+1)}
                })

            pathContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .attr({
                    // "id": "chart-zoombar",
                    "class": "chart-zoombar"
                    // "fill": "red"
                })
                .attr({
                    // "fill": function(d) { return _data2colors[d.category]},
                    "transform": "translate(" + (_w_group_name + w_gap ) + "," + (0) + ")",
                    "fill": function(d) { return _data2colors[d.category]; },
                    // "d": function(d) { return barFun(_x(w_bar_start_width))}
                    "d": function(d) { return barFun(_x(d.val))}
                })

            // update (grow up) chart-zoombar 
            pathContainer
                .select(".chart-zoombar")
                .transition() 
                .attr({
                    // "fill": function(d) { return _data2colors[d.category]},
                    "transform": "translate(" + (_w_group_name + w_gap +barZoom) + "," + (-barZoom) + ")",
                    // "fill": function(d) { return _data2colors[d.category]; },
                    // "d": function(d) { return barFun(_x(w_bar_start_width))}
                    // "d": function(d) { return barFun(_x(d.val))}
                })

            // make selected value bold    
            pathContainer
                .selectAll("text")
                .classed("chart-selected_value", true);


        }
    }

    function onMouseOut_Bar(d) {

        onMouseOut_Tolltip();

        var pathContainer = d3.select(d.parentNode);
        pathContainer
            .selectAll(".chart-zoombar")
            .transition().duration(50)
            .attr("transform", "translate(" + (_w_group_name + w_gap ) + "," + (0) + ")")
            .remove();
        pathContainer
            .selectAll(".chart-zoombar_shadow_dark")
            .remove();
        pathContainer
            .selectAll(".chart-zoombar_shadow")
            .remove();
        pathContainer
            .selectAll("text")
            .classed("chart-selected_value", false);


        // var thisBar = d3.select(d);
        // // console.log(thisBar);

        // thisBar
        // .transition().duration(g_duration/4)
        // .attr({
        //     "d": function(d) { return barFun(_x(d.val))},
        //     "fill": function(d) { return _data2colors[d.category]; }
        // })
    }

    function onMouseMove_Bar(d) {

        onMouseMove_Tolltip();

    }

    function onMouseOver_Tolltip(value) {if (_tooltipOn) return _tooltip.text(value).style("visibility", "visible");}
    function onMouseMove_Tolltip() {if (_tooltipOn) return _tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");}
    function onMouseOut_Tolltip() {if (_tooltipOn) return _tooltip.style("visibility", "hidden");}


    
    _chart.setSeries(cdata);
    _chart.render();
    return _chart;
}
