// @codekit-prepend "cdata3.js";

var all_colors = ["#21a7d6","#999999","#0086cd","#78bfe2", "#b1d8ed", "#cdcdcd", "#a2bba8", "#becec0","#dee6df","#0086cd","#8c7dae", "#cbc4de", "#efad63", "#f4c58f"];


var c01_02_data = {
    title: "Отраслевая структура кредитного портфеля банка развития, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"Факт на 01.01.2014",
            "group": [
            {"val": 49.2, "category": "Отрасли промышленности","offsetY":30,"offsetX":-50},
            {"val": 39.4, "category": "Секторы инфраструктуры","offsetY":15,"offsetX":20},
            {"val": 9.1, "category": "Агропромышленный комплекс","offsetY":10},
            {"val": 2.3, "category": "Прочие отрасли"}
            ]
        },

        { "group_name":"Стратегия (ориентир на\xA02015 год)",
            "group": [
            {"val": 58.0, "category": "Отрасли промышленности","offsetY":0,"offsetX":0},
            {"val": 34.0, "category": "Секторы инфраструктуры","offsetY":0,},
            {"val": 8.0, "category": "Агропромышленный комплекс","offsetY":-2}
            ]
        }
    ]
};
var en_c01_02_data = {
    title: "Development Bank’s Loan Portfolio: Sectoral Structure, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"Actual result as at 01.01.2014",
            "group": [
            {"val": 49.2, "category": "Industries","offsetY":30,"offsetX":-50},
            {"val": 39.4, "category": "Infrastructure sectors","offsetY":15,"offsetX":20},
            {"val": 9.1, "category": "Agricultural complex","offsetY":10},
            {"val": 2.3, "category": "Other sectors"}
            ]
        },

        { "group_name":"Strategy target for 2015",
            "group": [
            {"val": 58.0, "category": "Industries","offsetY":0,"offsetX":0},
            {"val": 34.0, "category": "Infrastructure sectors","offsetY":0},
            {"val": 8.0, "category": "Agricultural complex","offsetY":-2}
            ]
        }
    ]
};



var c02_01_data = {
    title: "Структура кредитного портфеля банка развития (в разрезе отраслей / секторов экономики), %",

    w_group_name: 70,
    legendHeight: 160,
    tooltip : false,
    legend_align: "left",
    le : [185,185,182],
    colors: ["#0086cd","#21a7d6","#78bfe2","#b1d8ed",  "#999999","#cdcdcd",   "#a2bba8", "#becec0","#dee6df",    "#8c7dae", "#cbc4de",     "#efad63", "#f4c58f"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 38.1, "category": "Секторы инфраструктуры","offsetY":79, "offsetX":-10},
            {"val": 11.0, "category": "Агропромышленный комплекс","offsetY":-2},
            {"val": 10.0, "category": "Химическая и нефтехимическая промышленность","offsetY":7},
            {"val": 7.8, "category": "Машиностроение (кроме\xA0авиастроения)","offsetY":7},
            {"val": 6.0, "category": "Авиастроение и авиаперевозки"},
            {"val": 8.3, "category": "Металлургия","offsetY":11},
            {"val": 2.2, "category": "Оборонно-промышленный комплекс","offsetY":25, "offsetX":45},
            {"val": 6.9, "category": "Промышленность строительных материалов","offsetY":32, "offsetX":30},
            {"val": 4.6, "category": "Деревообрабатывающая промышленность","offsetY":37, "offsetX":15},
            {"val": 2.2, "category": "Электронная промышленность","offsetY":31, "offsetX":5},
            {"val": 1.4, "category": "Судостроение","offsetY":21},
            {"val": 0.7, "category": "Медицинская техника и фармацевтика","offsetY":11},
            {"val": 0.2, "category": "Прочие отрасли"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 40.2, "category": "Секторы инфраструктуры","offsetY":74, "offsetX":-10},
            {"val": 9.2, "category": "Агропромышленный комплекс","offsetY":-5},
            {"val": 8.9, "category": "Химическая и нефтехимическая промышленность","offsetY":5},
            {"val": 6.7, "category": "Машиностроение (кроме\xA0авиастроения)"},
            {"val": 6.3, "category": "Авиастроение и авиаперевозки","offsetY":1},
            {"val": 7.8, "category": "Металлургия","offsetY":11},
            {"val": 3.6, "category": "Оборонно-промышленный комплекс","offsetY":15, "offsetX":40},
            {"val": 6.5, "category": "Промышленность строительных материалов","offsetY":25, "offsetX":30},
            {"val": 4.3, "category": "Деревообрабатывающая промышленность","offsetY":32, "offsetX":20},
            {"val": 2.4, "category": "Электронная промышленность","offsetY":27, "offsetX":7},
            {"val": 1.1, "category": "Судостроение","offsetY":18, "offsetX":3},
            {"val": 0.7, "category": "Медицинская техника и фармацевтика","offsetY":9, "offsetX":0},
            {"val": 2.3, "category": "Прочие отрасли"}
            ]
        }
    ]
};
var en_c02_01_data = {
    title: "Loans to customers (by industry/sector of economy), %",

    w_group_name: 70,
    legendHeight: 160,
    tooltip : false,
    legend_align: "left",
    le : [185,185,182],
    colors: ["#0086cd","#21a7d6","#78bfe2","#b1d8ed",  "#999999","#cdcdcd",   "#a2bba8", "#becec0","#dee6df",    "#8c7dae", "#cbc4de",     "#efad63", "#f4c58f"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 38.1, "category": "Infrastructure","offsetY":79, "offsetX":-10},
            {"val": 11.0, "category": "Agro-industrial complex","offsetY":-2},
            {"val": 10.0, "category": "Chemicals and petrochemicals","offsetY":7},
            {"val": 7.8, "category": "Machine engineering (except aircraft building)","offsetY":7},
            {"val": 6.0, "category": "Aircraft building and transportation"},
            {"val": 8.3, "category": "Metallurgy","offsetY":11},
            {"val": 2.2, "category": "Defense industry","offsetY":25, "offsetX":45},
            {"val": 6.9, "category": "Construction materials industry","offsetY":32, "offsetX":30},
            {"val": 4.6, "category": "Timber processing industry","offsetY":37, "offsetX":15},
            {"val": 2.2, "category": "Electronics","offsetY":31, "offsetX":5},
            {"val": 1.4, "category": "Shipbuilding","offsetY":21},
            {"val": 0.7, "category": "Medical equipment and pharmaceuticals","offsetY":11},
            {"val": 0.2, "category": "Other"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 40.2, "category": "Infrastructure","offsetY":74, "offsetX":-10},
            {"val": 9.2, "category": "Agro-industrial complex","offsetY":-5},
            {"val": 8.9, "category": "Chemicals and petrochemicals","offsetY":5},
            {"val": 6.7, "category": "Machine engineering (except aircraft building)"},
            {"val": 6.3, "category": "Aircraft building and transportation","offsetY":1},
            {"val": 7.8, "category": "Metallurgy","offsetY":11},
            {"val": 3.6, "category": "Defense industry","offsetY":15, "offsetX":40},
            {"val": 6.5, "category": "Construction materials industry","offsetY":25, "offsetX":30},
            {"val": 4.3, "category": "Timber processing industry","offsetY":32, "offsetX":20},
            {"val": 2.4, "category": "Electronics","offsetY":27, "offsetX":7},
            {"val": 1.1, "category": "Shipbuilding","offsetY":18, "offsetX":3},
            {"val": 0.7, "category": "Medical equipment and pharmaceuticals","offsetY":9, "offsetX":0},
            {"val": 2.3, "category": "Other"}
            ]
        }
    ]
};


var c02_02_data = {
    title: "Структура кредитного портфеля банка развития (в разрезе срочности), %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [129,139,159,120],
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 88.4, "category": "Свыше 5 лет"},
            {"val": 8.6, "category": "От 3 до 5 лет","offsetY":30, "offsetX":15},
            {"val": 1.0, "category": "От 1 года до 3 лет","offsetY":17, "offsetX":1},
            {"val": 2.0, "category": "До 1 года"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 90.4, "category": "Свыше 5 лет"},
            {"val": 7.3, "category": "От 3 до 5 лет","offsetY":30, "offsetX":8},
            {"val": 1.2, "category": "От 1 года до 3 лет","offsetY":17},
            {"val": 1.1, "category": "До 1 года"}
            ]
        }
    ]
};
var en_c02_02_data = {
    title: "Loans to customers (by maturity), %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [129,139,159,120],
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 88.4, "category": "Above 5 years"},
            {"val": 8.6, "category": "3 to 5 years","offsetY":30, "offsetX":15},
            {"val": 1.0, "category": "1 to 3 years","offsetY":17, "offsetX":1},
            {"val": 2.0, "category": "Up to 1 year"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 90.4, "category": "Above 5 years"},
            {"val": 7.3, "category": "3 to 5 years","offsetY":30, "offsetX":8},
            {"val": 1.2, "category": "1 to 3 years","offsetY":17},
            {"val": 1.1, "category": "Up to 1 year"}
            ]
        }
    ]
};

var c02_04_data = {
    title: "Структура портфеля поддержки, предоставленной партнерами субъектам МСП, в разрезе срочности кредитов, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [129,139,159,120],

    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 56.24, "category": "Свыше 3 лет","offsetY":25, "offsetX":-90},
            {"val": 15.85, "category": "От 2 до 3 лет"},
            {"val": 10.53, "category": "От 1 года до 2 лет"},
            {"val": 10.43, "category": "До 1 года"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 72.25, "category": "Свыше 3 лет","offsetY":10, "offsetX":-120},
            {"val": 22.80, "category": "От 2 до 3 лет"},
            {"val": 10.52, "category": "От 1 года до 2 лет","offsetY":32, "offsetX":10},
            {"val": 1.38, "category": "До 1 года","offsetY":5}
            ]
        }
    ]
};
var en_c02_04_data = {
    title: "Portfolio of loans extended to SMEs by partners: maturity structure, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [129,139,159,120],
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 56.24, "category": "Over 3 years","offsetY":25, "offsetX":-90},
            {"val": 15.85, "category": "2 to 3 years"},
            {"val": 10.53, "category": "1 to 2 years"},
            {"val": 10.43, "category": "Up to 1 year"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 72.25, "category": "Over 3 years","offsetY":10, "offsetX":-120},
            {"val": 22.80, "category": "2 to 3 years"},
            {"val": 10.52, "category": "1 to 2 years","offsetY":32, "offsetX":10},
            {"val": 1.38, "category": "Up to 1 year","offsetY":5}
            ]
        }
    ]
};


// график с разделенным сектором

var c02_05_data = {
    title: "Структура портфеля поддержки, предоставленной партнерами субъектам МСП, по стратегическим нишам, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [230,195,130],
    w_line : 70,
    line : [{
                category: "Кредитование инновационных МСП",
                line_grad : 120,
                texts : [
                    { extra_text : "<*>\xA0\xA0\xA04,83 Инновации",
                      extra_y : 40,
                      extra_w : 107,
                      linestart: 35

                    },
                    { extra_text : "<*>\xA0\xA0\xA057,55 Модернизация и\xA0энергоэффективность",
                      extra_y : 75,
                      extra_w : 110,
                      linestart: 45

                    }
                ]
            },
            {
                category: "Кредитование инновационных МСП",
                line_grad : 108,
                texts : [
                    { extra_text : "<*>\xA0\xA0\xA03,47 Инновации",
                      extra_y : 40,
                      extra_w : 119,
                      linestart: 35

                    },
                    { extra_text : "<*>\xA0\xA0\xA055,33 Модернизация и\xA0энергоэффективность",
                      extra_y : 75,
                      extra_w : 130,
                      linestart: 45

                    }
                ]
            }
    ],
    colors: ["#21a7d6","#999999","#0086cd"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 62.38, "category": "Кредитование инновационных МСП","offsetY":-10, "offsetX":-80}, //  4.83 Инновации,  57.55 Модернизация и энергоэффективность
            {"val": 18.15, "category": "Прочие стратегические ниши","offsetY":-20},
            {"val": 19.47, "category": "Иные средства"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.80, "category": "Кредитование инновационных МСП","offsetY":-2, "offsetX":-78},//  3,47 Инновации,  55.33 Модернизация и энергоэффективность
            {"val": 24.55, "category": "Прочие стратегические ниши","offsetY":-25},
            {"val": 16.65, "category": "Иные средства","offsetY":3}
            ]
        }
    ]
};
var en_c02_05_data = {
    title: "Portfolio of loans extended to SMEs by partners: strategic niche structure, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    le : [230,195,130],
    legend_align: "left",
    colors: ["#21a7d6","#999999","#0086cd"],
    w_line : 70,
    extra_text_width : 70,
    line : [{
                category: "Loans to innovative SMEs",
                line_grad : 120,
                texts : [
                    { extra_text : "<*>\xA0\xA0\xA04.83 Innovations",
                      extra_y : 40,
                      extra_w : 107,
                      linestart: 35

                    },
                    { extra_text : "<*>\xA0\xA0\xA057.55 Modernization and energy efficiency",
                      extra_y : 75,
                      extra_w : 110,
                      linestart: 45

                    }
                ]
            },
            {
                category: "Loans to innovative SMEs",
                line_grad : 108,
                texts : [
                    { extra_text : "<*>\xA0\xA0\xA03.47 Innovations",
                      extra_y : 40,
                      extra_w : 119,
                      linestart: 35

                    },
                    { extra_text : "<*>\xA0\xA0\xA055.33 Modernization and energy efficiency",
                      extra_y : 75,
                      extra_w : 130,
                      linestart: 45

                    }
                ]
            }
    ],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 62.38, "category": "Loans to innovative SMEs","offsetY":-10, "offsetX":-50}, //  4.83 Innovations,  57.55 Modernization and energy efficiency
            {"val": 18.15, "category": "Other strategic niches","offsetY":-20},
            {"val": 19.47, "category": "Other resources"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.80, "category": "Loans to innovative SMEs","offsetY":-2, "offsetX":-50},//  3,47 Innovations,  55.33 Modernization and energy efficiency
            {"val": 24.55, "category": "Other strategic niches","offsetY":-25},
            {"val": 16.65, "category": "Other resources","offsetY":3}
            ]
        }
    ]
};


var c03_02_data = {
    title: "Структура портфеля ценных бумаг, %",

    w_group_name: 70,
    legendHeight: 90,
    tooltip : false,
    legend_align: "left",
    le : [280,310],
    colors: ["#21a7d6","#999999","#78bfe2","#0086cd"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 45.9, "category": "Государственные долговые обязательства","offsetY":70, "offsetX":-30},
            {"val": 47.1, "category": "Акции и депозитарные расписки","offsetY":-25, "offsetX":10},
            {"val": 5.9, "category": "Корпоративные долговые обязательства резидента РФ (облигации, еврооблигации и\xA0векселя)","offsetY":20, "offsetX":5},
            {"val": 1.1, "category": "Другие долговые обязательства (облигации иностранных эмитентов и кредитные ноты, «привязанные» к корпоративному и\xA0суверенному\xA0риску)"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 53.7, "category": "Государственные долговые обязательства","offsetY":49, "offsetX":-60},
            {"val": 41.1, "category": "Акции и депозитарные расписки","offsetY":0, "offsetX":20},
            {"val": 3.0, "category": "Корпоративные долговые обязательства резидента РФ (облигации, еврооблигации и\xA0векселя)","offsetY":20, "offsetX":5},
            {"val": 2.2, "category": "Другие долговые обязательства (облигации иностранных эмитентов и кредитные ноты, «привязанные» к корпоративному и\xA0суверенному\xA0риску)"}
            ]
        }
    ]
};
var en_c03_02_data = {
    title: "Securities portfolio, %",

    w_group_name: 70,
    legendHeight: 90,
    tooltip : false,
    legend_align: "left",
    le : [250,300],
    colors: ["#21a7d6","#999999","#78bfe2","#0086cd"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 45.9, "category": "Russian Federation debt securities","offsetY":70, "offsetX":-30},
            {"val": 47.1, "category": "Shares and depository receipts","offsetY":-25, "offsetX":10},
            {"val": 5.9, "category": "Corporate debt securities of the Russian Federation residents (bonds, Eurobonds and promissory notes)","offsetY":20, "offsetX":5},
            {"val": 1.1, "category": "Other debt securities (bonds of foreign issuers and credit notes linked to the corporate and sovereign risks)"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 53.7, "category": "Russian Federation debt securities","offsetY":49, "offsetX":-60},
            {"val": 41.1, "category": "Shares and depository receipts","offsetY":0, "offsetX":20},
            {"val": 3.0, "category": "Corporate debt securities of the Russian Federation residents (bonds, Eurobonds and promissory notes)","offsetY":20, "offsetX":5},
            {"val": 2.2, "category": "Other debt securities (bonds of foreign issuers and credit notes linked to the corporate and sovereign risks)"}
            ]
        }
    ]
};



var c06_01_data = {
    title: "Структура расширенного инвестиционного портфеля, %",

    w_group_name: 70,
    legendHeight: 120,
    tooltip : false,
    le : [190,190,170],
    legend_align: "left",
    colors: ["#0086cd","#21a7d6","#78bfe2","#b1d8ed",  "#999999","#cdcdcd",   "#a2bba8","#becec0",    "#8c7dae"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 25.0, "category": "Облигации федерального займа","offsetY":-30, "offsetX":-40},
            {"val": 32.5, "category": "Государственные сберегательные облигации","offsetY":16, "offsetX":-30},
            {"val": 0.3, "category": "Облигации субъектов\xA0РФ"},
            {"val": 6.1, "category": "Корпоративные облигации (гарантированные РФ)","offsetY":-5, "offsetX":0},
            {"val": 9.8, "category": "Корпоративные облигации"},
            {"val": 2.2, "category": "Облигации с ипотечным покрытием"},
            {"val": 1.6, "category": "Облигации международных финансовых организаций"},
            {"val": 21.1, "category": "Денежные средства"},
            {"val": 1.4, "category": "НКД","offsetY":8, "offsetX":0}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 19.7, "category": "Облигации федерального займа", "offsetY":-22, "offsetX":-35},
            {"val": 24.9, "category": "Государственные сберегательные облигации","offsetY":48, "offsetX":-38},
            {"val": 0.4, "category": "Облигации субъектов\xA0РФ","offsetY":-7, "offsetX":-3},
            {"val": 5.5, "category": "Корпоративные облигации (гарантированные РФ)","offsetY":-24, "offsetX":-5},
            {"val": 25.8, "category": "Корпоративные облигации","offsetY":-26},
            {"val": 3.1, "category": "Облигации с ипотечным покрытием"},
            {"val": 1.5, "category": "Облигации международных финансовых организаций"},
            {"val": 17.8, "category": "Денежные средства","offsetY":7, "offsetX":-3},
            {"val": 1.3, "category": "НКД","offsetY":7, "offsetX":-0}
            ]
        }
    ]
};
var en_c06_01_data = {
    title: "Structure of extended investment portfolio, %",

    w_group_name: 70,
    legendHeight: 120,
    tooltip : false,
    le : [190,190,170],
    legend_align: "left",
    colors: ["#0086cd","#21a7d6","#78bfe2","#b1d8ed",  "#999999","#cdcdcd",   "#a2bba8","#becec0",    "#8c7dae"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 25.0, "category": "OFZs","offsetY":-30, "offsetX":-40},
            {"val": 32.5, "category": "GSOs","offsetY":16, "offsetX":-30},
            {"val": 0.3, "category": "Russian Federation constituent entities’ bonds"},
            {"val": 6.1, "category": "Corporate bonds (guaranteed by the Russian Federation)","offsetY":-5, "offsetX":0},
            {"val": 9.8, "category": "Corporate bonds"},
            {"val": 2.2, "category": "Mortgage-backed bonds"},
            {"val": 1.6, "category": "IFO bonds"},
            {"val": 21.1, "category": "Cash"},
            {"val": 1.4, "category": "Accrued coupon","offsetY":8, "offsetX":0}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 19.7, "category": "OFZs", "offsetY":-22, "offsetX":-35},
            {"val": 24.9, "category": "GSOs","offsetY":48, "offsetX":-38},
            {"val": 0.4, "category": "Russian Federation constituent entities’ bonds","offsetY":-7, "offsetX":-3},
            {"val": 5.5, "category": "Corporate bonds (guaranteed by the Russian Federation)","offsetY":-24, "offsetX":-5},
            {"val": 25.8, "category": "Corporate bonds","offsetY":-26},
            {"val": 3.1, "category": "Mortgage-backed bonds"},
            {"val": 1.5, "category": "IFO bonds"},
            {"val": 17.8, "category": "Cash","offsetY":7, "offsetX":-3},
            {"val": 1.3, "category": "Accrued coupon","offsetY":7, "offsetX":-0}
            ]
        }
    ]
};





var c06_02_data = {
    title: "Инвестиционный портфель ГЦБ, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [76,76,76,185,170],
    colors: ["#0086cd","#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 35.4, "category": "ГСО","offsetY":-49, "offsetX":-60},
            {"val": 21.2, "category": "ОФЗ"},
            {"val": 35.2, "category": "НКД"},
            {"val": 6.6, "category": "Корпоративные облигации","offsetY":20, "offsetX":7},
            {"val": 1.6, "category": "Денежные средства","offsetY":10, "offsetX":0}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 25.9, "category": "ГСО","offsetY":-32, "offsetX":-45},
            {"val": 33.9, "category": "ОФЗ","offsetY":5, "offsetX":-30},
            {"val": 31.3, "category": "НКД","offsetY":2},
            {"val": 7.4, "category": "Корпоративные облигации","offsetY":16, "offsetX":7},
            {"val": 1.5, "category": "Денежные средства","offsetY":7, "offsetX":0}
            ]
        }
    ]
};
var en_c06_02_data = {
    title: "Government Securities Investment Portfolio, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [90,90,145,145,90],
    colors: ["#0086cd","#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 35.4, "category": "GSOs","offsetY":-49, "offsetX":-60},
            {"val": 21.2, "category": "OFZs"},
            {"val": 35.2, "category": "Accrued сoupon"},
            {"val": 6.6, "category": "Corporate bonds","offsetY":20, "offsetX":7},
            {"val": 1.6, "category": "Cash","offsetY":10, "offsetX":0}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 25.9, "category": "GSOs","offsetY":-32, "offsetX":-45},
            {"val": 33.9, "category": "OFZs","offsetY":5, "offsetX":-30},
            {"val": 31.3, "category": "Accrued сoupon","offsetY":2},
            {"val": 7.4, "category": "Corporate bonds","offsetY":16, "offsetX":7},
            {"val": 1.5, "category": "Cash","offsetY":7, "offsetX":0}
            ]
        }
    ]
};






var c06_03_data = {
    title: "Структура портфеля средств выплатного резерва, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    le : [86,190,165,76],
    legend_align: "left",
    colors: ["#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 40.8, "category": "ОФЗ","offsetX":-60},
            {"val": 13.1, "category": "Корпоративные облигации"},
            {"val": 45.1, "category": "Денежные средства"},
            {"val": 1.0, "category": "НКД"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.7, "category": "ОФЗ", "offsetX":-60},
            {"val": 35.5, "category": "Корпоративные облигации"},
            {"val": 4.0, "category": "Денежные средства","offsetY":15, "offsetX":3},
            {"val": 1.8, "category": "НКД"}
            ]
        }
    ]
};
var en_c06_03_data = {
    title: "Structure of the Payment Reserve Portfolio, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    le : [115,160,115,160],
    legend_align: "left",
    colors: ["#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 40.8, "category": "OFZs","offsetX":-60},
            {"val": 13.1, "category": "Corporate bonds"},
            {"val": 45.1, "category": "Cash"},
            {"val": 1.0, "category": "Accrued coupon"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.7, "category": "OFZs", "offsetX":-60},
            {"val": 35.5, "category": "Corporate bonds"},
            {"val": 4.0, "category": "Cash","offsetY":15, "offsetX":3},
            {"val": 1.8, "category": "Accrued coupon"}
            ]
        }
    ]
};






var c06_04_data = {
    title: "Структура портфеля средств пенсионных накоплений застрахованных лиц, которым установлена срочная пенсионная выплата, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [86,190,165,76],
    colors: ["#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 45.4, "category": "ОФЗ","offsetX":-50},
            {"val": 13.5, "category": "Корпоративные облигации"},
            {"val": 39.8, "category": "Денежные средства","offsetY":-10},
            {"val": 1.3, "category": "НКД"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.7, "category": "ОФЗ","offsetX":-60},
            {"val": 35.5, "category": "Корпоративные облигации"},
            {"val": 4.0, "category": "Денежные средства","offsetY":20, "offsetX":5},
            {"val": 1.8, "category": "НКД"}
            ]
        }
    ]
};

var en_c06_04_data = {
    title: "Structure of the Portfolio of Pension Savings of the Ensured Citizens Entitled to a Term Pension Payment, %",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    le : [115,160,115,160],
    colors: ["#21a7d6","#999999","#b1d8ed","#78bfe2"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 45.4, "category": "OFZs","offsetX":-50},
            {"val": 13.5, "category": "Corporate bonds"},
            {"val": 39.8, "category": "Cash","offsetY":-10},
            {"val": 1.3, "category": "Accrued coupon"}
            ]
        },

        { "group_name":"01.01.2014",
            "group": [
            {"val": 58.7, "category": "OFZs","offsetX":-60},
            {"val": 35.5, "category": "Corporate bonds"},
            {"val": 4.0, "category": "Cash","offsetY":20, "offsetX":5},
            {"val": 1.8, "category": "Accrued coupon"}
            ]
        }
    ]
};




/*=================================
=          Global Functions       =
=================================*/


function randomData() {
    return Math.random() * 9;
}

/*-----  End of Global Functions  ------*/

function pieChart(place,cdata,w) {

    var uid = generateUID(),

        base_place = "body",

        chart_fullwidth = 538,
        barHeight = 18,
        barZoom = 5, // zoom distance
        _bs = 20, // size of BID shadow under pie chart
        barHeightBig = barHeight+barZoom,
        skos = barHeight/2,
        // bigskos = barHeightBig/2.2,
        // placefortext = 30,
        // line_width = 50,
        mtop = 50,
        mbottom = 50,
        // dist2line = 30,
        // start_text = 50,
        // bar_width = fullwidth - start_text - dist2line - line_width - placefortext,
        dot_line_color = "#575757",
        dot_size = 2,
        stroke_width = 0.7,
        gap_between_bars = 0,
        gap_between_groups = 10,
        gap_to_legend = 30,

        h_legend_text = 10,
        legend_line_height = 6,

        g_duration = 1000,

        w_line = 50,
        w_value_one_character = 5,  // width of one character in value field
        //total width of field = w_value + w_value_one_character * d.val length
        w_gap = 10,
        w_gap_between_legends = 3*w_gap,
        w_bar_start_width = 10,   // minimum width of bar to start growth animation
        w_legend_bar = 22;

    var _pieWidth = w,  // set initial width og pie chart to w
        _radius = 1,
        _arc,  _arc2,  // set in renderBody()
        _arc_radius_modifier = 0.7,
        _pie;  // set in renderBody()

    var fadeLevel = 0.2, // level of fading of charts
        Time2KeepCategoryActivated = 5000;  // 4 sec keep legent category activated after click and activate all after that

        var ru_RU = {
          "decimal": ",",
          "thousands": "\xa0",
          "grouping": [3],
          "currency": ["", " руб."],
          "dateTime": "%A, %e %B %Y г. %X",
          "date": "%d.%m.%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
          "shortDays": ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
          "months": ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
          "shortMonths": ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
        };
        var en_US ={
          "decimal": ".",
          "thousands": ",",
          "grouping": [3],
          "currency": ["$", ""],
          "dateTime": "%a %b %e %X %Y",
          "date": "%m/%d/%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        };

        var d3RU = d3.locale(ru_RU),
            d3EN = d3.locale(en_US),
            local_num_format = d3RU.numberFormat();
            // local_num_format = d3EN.numberFormat();

    /*-----  End of Globals  ------*/



    w = (typeof w === "undefined") ? chart_fullwidth : w;   // if no w set, default value is chart_fullwidth

    var _chart = {},

        _width = w, // to change width of chart dynamicaly we have to pass w to function
        _height = 300,  // full height chart+legend (start with free value 300px)
        // _margins = {top: 0, left: 0, right: 0, bottom: 0},
        _x, _y,
        _data = [],
        _data2colors = [],
        // _colors = d3.scale.category10(),
        _svg,
        _bodyG,

        _max_data_value = 0,

        _groups_total = 0,
        _bars_total = 0,
        _height_chart = 0,  // height of chart part without legend
        _legendSpace = 0,  // horizontal width of one legend block
        _legendHeight = barHeight,  // starting value for height of legend block
        _legendAlign = "left",

        _legendTextWidth = [],
        _legendTextHeight = [],  // number of lines in each legend item
        _group_names_TextWidth = [],
        _le = [],

        _categoryNames = [],  // list of unique data.group.categories
        _categoryMap = [],  // list on category objects for chart legend: {category,active}

        _arcs = [],
        _lines = [],

        _line,

        _tooltip,
        _timerId,
        _w_group_name,
        _w_bar,
        _w_value = 5;  // base width of value field;

    var _tag_replace = /[|&;$%@"<>()+,. \xA0]/g;  // regex to replace invalid characters in selector name




    var barFun = function(x) {
        return ["m",[skos,0],
        "l",[x-skos,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[-x+skos,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],
        "Z"].join(" ");
        }

    var barFunLeft = function(x) {
        return ["m",[skos,0],
        "l",[0,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[0,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],
        "Z"].join(" ");
        }

    function generateUID() {
        return ("0000" + (Math.random()*Math.pow(36,4) << 0).toString(36)).slice(-4)
    }

    function ColorLuminance(hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i*2,2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00"+c).substr(c.length);
        }

        return rgb;
    }


    function quadrantWidth() {
        // return _width - _margins.left - _margins.right;
        return _width;

    }

    function quadrantHeight() {
        // return _height - _margins.top - _margins.bottom;
        return _height;

    }


    //.setSeries – set data and prepare corresponding vars
    _chart.setSeries = function (series) {
        _data = series.data;
        // _data2colors = colors;
        _legendHeight = series.legendHeight;
        _legendAlign = (typeof series.legend_align === "undefined") ? "left" : series.legend_align;
        w_line = (typeof series.w_line === "undefined") ? w_line : series.w_line;
        _extra_text_width = (typeof series.extra_text_width === "undefined") ? 50 : series.extra_text_width;
        _w_group_name = series.w_group_name;
        // _w_bar = _width - ( _w_group_name + w_gap + w_gap + w_line + w_gap + _w_value);



        _groups_total = _data.length;  // get number of pie charts

        _max_data_value = 0;
        // calculate max value of ALL val
        _max_data_value = d3.max(_data,
        function(d) {

            return d3.max(d.group, function(d) {
                // var  prec = (d.val + "").split("."),
                //      round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;

                // console.log(d.val, (d.val + "").length, prec, round);
                return parseFloat(d.val); })
        });

        _bars_total = 0;
        _data.forEach(function(d,i) {
        //     // calculate total number of bars
            d.group.forEach(function(dd,ii){
                // store current number to NUM property of each data element
                dd.num = _bars_total++;
            })
        })


        // add initial values for arc tween animation
        // set all values to 0 and the last value to any nonzero
        // so we will see how pie sectors grow from initial to real values
        // pushing last sector from 100% to its real value
        _data.forEach(function(d,i) {
            var last = 0;
        // add initial values for arc tween animation
            d.group.forEach(function(dd,ii){
                dd.initial = 0;
                last = ii;
            })
            d.group[last].initial = 1;  // set lat value to huge to fill the whole pie
        })
        // console.log(_data);

        /*==========  prepare legend  ==========*/

        // clear legend arrays
        _categoryNames.length = 0;
        _categoryMap.length = 0;

        //create list of unique data.group.categories

        // data2.forEach(group.forEach( function(d) {categoryNames[d.category] = d;});
        _data.forEach( function(d){
            d.group.forEach( function(dd){_categoryNames.push(dd.category);} )  // collect all category names
        });
        _categoryNames = d3.set(_categoryNames).values();  // keep unique names only

        var tmpcolors = [];

        // for every unique category create object
        // to control if category of legend is active or not
        for (var ii = 0; ii < _categoryNames.length; ii++) {
            _categoryMap.push({
                category: _categoryNames[ii],
                active: true
            })

            // set colors for each category
            // set RED color for all categories with no color specified
            _data2colors[_categoryNames[ii]] = (ii<series.colors.length) ? series.colors[ii] : "red";

        }

        _legendSpace = ( _width - (w_gap-1)*_categoryMap.length ) / _categoryMap.length;
        // console.log(_legendSpace);

        _le = series.le;


        /*==========  set widths  ==========*/

        _pieWidth = ( w - gap_between_groups*(_groups_total-1) ) / _groups_total;

        _radius = (_pieWidth - _w_value - w_gap - w_line - barZoom) / 2;
        // _w_group_name + w_gap + w_gap + w_line + w_gap + _w_value


        /*==========  set heights  ==========*/

        // height of chart part
        // _height_chart = barZoom + barHeight * _bars_total + gap_between_bars * (_bars_total - 1) + gap_between_groups * (_groups_total - 1);
        _height_chart = _pieWidth;



        // _line = series.line;
        _line = (typeof series.line === "undefined") ? null : series.line;



        // total height of svg: height of chart + height of legend block
        _height = _height_chart + gap_to_legend + _legendHeight;  // add place for legend

        // console.log(_height_chart);
        _tooltipOn = series.tooltip;

        if(RegExp(/en/).test(place)) local_num_format = d3EN.numberFormat();

        return _chart;
    };


    // .render – render chart
    _chart.render = function () {

        if (!_data.length) {console.log("ERROR: Set data with .setSeries before running .render"); return false;}
        if (!_svg) {  _svg = d3.select(base_place).select(place).append("svg");}
        if (!_tooltip) {  _tooltip = d3.select(base_place).select(place).append("div");}

        _svg
            // .transition().duration(200)
            .attr("height", _height)
            .attr("width", _width);

        _x = d3.scale.linear()
            .domain([0, _max_data_value])
            .range([w_bar_start_width, _w_bar]);


        // renderAxes(_svg);

        defineBodyClip(_svg);
        renderBody(_svg);
    };

    function defineBodyClip(svg) {
        var padding = 5,
            bodyclipplace = place+"_body-clip";

        svg.select("defs")
        .remove("defs");

        svg.append("defs")
                .append("clipPath")
                .attr("id", bodyclipplace)
                .append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", quadrantWidth() + 2 * padding)
                .attr("height", quadrantHeight());
    }

    function renderBody(svg) {
        var bodyclipplace = place+"_body-clip";
        if (!_bodyG)
            _bodyG = svg.append("g")
                    // .attr("class", "body")
                    // .attr("transform", "translate("
                    //         + xStart()
                    //         + ","
                    //         + yEnd() + ")")
                    .attr("clip-path", "url("+bodyclipplace+")");

        _arc = d3.svg.arc()
            .outerRadius(_radius)
            .innerRadius(0);

        _arc2 = d3.svg.arc()
            .outerRadius(_radius)
            .innerRadius(_radius*_arc_radius_modifier);

        _pie = d3.layout.pie()
            .sort(null)
            // .value(function(d) { return d.val; });
            .value(function(d) { return d.initial; });


        renderPies();
        renderLegend();
        renderTooltip();
    }

    function renderTooltip(){
        _tooltip
            .classed("chart-tooltip",true)
            .style("visibility", "hidden");
    }






    function renderPies() {
        // var padding = 2; // <-A
        // var tmp_height = barZoom;   // move first bar down for zoomBar to fit it zoomed version
        var tmp_height = 0;
        var tmp_counter = 0;


        // add groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            .enter()
            .append("g")
            .attr("class",function(d){return "c__" + uid  + "_" + tmp_counter++;})
            .classed('chart-groups',true);

        // update groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            // .transition()
            .attr("transform", function(d, i) {
                return "translate(" + ( (i+1)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups )) + "," + _pieWidth/2 +  ")";
            });


        // add big shadow
        var xx = _radius/Math.SQRT2,
            // big_shadow_path = "M"+xx+","+xx+"l"+(-_bs)+","+_bs+"A"+_radius+","+_radius+" 0 1,1 " +(-xx-_bs)+","+(-xx+_bs)+"l"+_bs+","+(-_bs)+"L0,0Z";
            big_shadow_path = "M"+xx+","+xx+"l"+(-_bs)+","+_bs+"A"+_radius+","+_radius+" 0 1,1 " +(-xx-_bs)+","+(-xx+_bs)+"l"+_bs+","+(-_bs)+"A"+_radius+","+_radius+" 0 1,1 " +(xx)+","+(xx)+"L0,0Z";

        _bodyG.selectAll("g.chart-groups")
            .append("path")
            .classed("big_shadow",true)
            .attr("fill","#0086cd")
            .attr("d", big_shadow_path)



        // add bars
        var barContainer = _bodyG.selectAll("g.chart-groups")
            .selectAll("g.chart-bars")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("g");


        var j = 0, jj = 0, jjj = 0;
        barContainer
            // .attr("id", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, ''); })
            .attr("class", function(d,i,ii) {

                // console.log(i,ii);
                // if (!_arcs[ii]) {_arcs[ii] = []}
                // _arcs[ii].push({

                //     "num":j,
                //     "classname":"tag" + uid + d.data.category.replace(_tag_replace, ''),
                //     "g_arc":d,
                //     "arc":null

                // });
                // j++;

                // if (!_arcs[ii]) {_arcs[ii] = []}
                _arcs.push({

                    "num":j,
                    "classname":"tag" + uid + d.data.category.replace(_tag_replace, ''),
                    "g_arc":d,
                    "arc":null

                });
                j++;

                // if (_line) {
                //     _line.forEach( function(dd,ii) {
                //         if (dd.category == d.data.category) {
                //             console.log("---------",_arcs[j-1]);
                //             console.log(dd,ii);
                //         }
                //     })
                // }

                return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num;
            })
            .classed('chart-bars', true)
            // .attr("transform", function(d, i) { return "translate(0," + (i * barHeight + gap_between_bars) +  ")"; })

        //draw bars with starting w_bar_start_width --- as set by DOMAIN/RANGE: _x(0) = w_bar_start_width
            .append("path")
            .attr('class',function(d,i,ii) {
                 return"chart-paths num" + uid + "_" + d.data.num;
            })
            .attr("d", _arc)
            .attr({
                // "transform": "translate(" + (_w_group_name + w_gap) + ",0)",

                // set real color
                // "fill": function(d) { return _data2colors[d.data.category]; }

                // set color to white. color real value during update
                "fill": function(d) { return "white"; }

                // "d": function(d) { return _arc;}
            })
            .each(function(d) {
                // _arcs[ii][i].arc = d;
                _arcs[jj++].arc = d;

                // console.log(ii,i,_arcs);
                this._current = d;
            })
            .on("mouseover", function(d) {return onMouseOver_Bar(this)})
            .on("mousemove", function(d) {return onMouseMove_Bar(this)})
            .on("mouseout", function(d) {return onMouseOut_Bar(this)});


        // add group for zoom elements
        _bodyG.selectAll("g.chart-groups")
            .append("g")
            .attr('class', 'chart-groups-zoom');


        // add values
        // set values to initial zero
         _bodyG.selectAll("g.chart-groups")
            .selectAll("text")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("text")
                // .attr("x", function(d) { return x(d.val) + 50; })
                // .attr("x", function(d, i) {
                //     return -((i+1)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups ));
                // })
                // .attr("y", function(d, i) {
                //     return w_gap - _radius;
                // })
                .attr("dy", ".35em")
                .attr("text-anchor","start")
                .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; })
                .classed('chart-value_text',true)
                .text(function(d) { return 0; });


        // add dots on lines
         _bodyG.selectAll("g.chart-groups")
            .selectAll("rect")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("rect")
                 // .attr("transform", function(d) { return "translate(" + (_w_group_name + w_gap + _x(w_bar_start_width) + w_gap) + "," + (barHeight - dot_size)/2 + ")"; })
                 // .attr("width", dot_size)
                 // .attr("height", dot_size)
                 // .style("fill","#999999");
                 .style("fill",dot_line_color)
                 .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });

        // add lines with starting length  according to starting w_bar_start_width
         _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("line")
                // .transition().duration(2000)
                // .attr("x1", function(d) { return _w_group_name + w_gap + _x(w_bar_start_width) + w_gap;})
                // .attr("y1", barHeight/2)
                // .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)})
                // .attr("y2", barHeight/2)
                .attr("stroke-width", stroke_width)
                // .attr("stroke", "#999999");
                .attr("stroke", dot_line_color)
                .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });


        // add group names
        _bodyG.selectAll("text.chart-group_text")
            .data(_data)
            .enter()
            .append("text")
            .attr("x", 0)
            .attr("y", w_gap)
            .attr("dy", ".35em")
            .attr('class', 'chart-group_text')
            .attr("transform", function(d, i) {
                return "translate(0,0)"; })
            .text(function(d) { return d.group_name});


        updatePies();


        if (_line){

            // _line = series.line;

            var extra_tmp = [];
            // var tt1 = "M0,0l50,50Z";
            var extra_tag = "extra",
                extra_y = 40;

            // var tt_х1 = 0;
            // Math.cos(_line) * _radius;
            // var tt_у1 = Math.sin(_line) * _radius;

            // console.log(_line, _radius, tt_x1, tt_y1);


            _bodyG.selectAll("g.chart-groups")
                .selectAll("path.dot_path")
                .data(function(d,i){return [_line[i]]; })
                .enter()
                .append("path")
                // .attr("class", function(d) { return "tag" + uid + d.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; })
                .classed("dot_path",true)
                .classed("extra", true)
                .attr("stroke","white")
                .attr("stroke-width",0.7)
                .attr("stroke-dasharray","5,10")
                .attr("d", function(d){
                    var gr = d.line_grad * Math.PI / 180,
                    tt1 = "M0,0L"+(Math.cos(gr) * _radius)+","+(Math.sin(gr) * _radius)+"Z";
                    return tt1;
                })
                // .attr("transform", function(d, i) {
                //     return "translate(431,132)"; })

            // _linetexts = _line[0].texts;
            _bodyG.selectAll("g.chart-groups")
                .selectAll("text.extra")
                .data(function(d,i){return _line[i].texts; })
                .enter()
                .append("text")
                    // .attr("transform", function(d, i) {
                    //     return "translate(431,132)"; })
                    .attr("x", -(_w_value + w_gap + w_line + _radius) )
                    .attr("y", function(d){return d.extra_y})
                    // .attr("x", function(d, i) {
                    //     return -((i+1)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups ));
                    // })
                    // .attr("y", function(d, i) {
                    //     return w_gap - _radius;
                    // })
                    .attr("dy", ".35em")
                    .attr("text-anchor","start")
                    // .attr("class", "tag" + extra_tag.replace(_tag_replace, '') + " num" + uid + "_" )
                    .attr("class", "tag" + extra_tag.replace(_tag_replace, '') + " num" + uid + "_" )
                    .classed('chart-value_text',true)
                    .classed("extra", true)
                    .text(function(d){return d.extra_text})
                    .call(wrap, _extra_text_width, -(_w_value + w_gap + w_line + _radius), extra_tmp, _le, false)
                    .call(textstyle);

            // append extra lines
            _bodyG.selectAll("g.chart-groups")
                .selectAll("line.extra")
                .data(function(d,i){return _line[i].texts; })
                .enter()
                .append("line")
                    .attr("class", "tag" + extra_tag.replace(_tag_replace, '') + " num" + uid + "_" )
                    .classed("extra", true)
                   // .transition().duration(2000)
                   .attr("x1", function(d) { return - (_w_value + w_gap + w_line + _radius - d.linestart)})
                   .attr("y1", function(d) { return d.extra_y})
                   .attr("x2", function(d) { return d.extra_w - (_w_value + w_gap + w_line + _radius - d.linestart)})
                   .attr("y2", function(d) { return d.extra_y})
                   .attr("stroke-width", stroke_width)
                   // .attr("stroke", "#999999");
                   .attr("stroke", dot_line_color)
                   // .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });


            // add dots on lines
            _bodyG.selectAll("g.chart-groups")
                .selectAll("rect.extra")
                .data(function(d,i){return _line[i].texts; })
                .enter()
                .append("rect")
                    .attr("class", "tag" + " num" + uid + "_" )
                    .classed("extra", true)
                     .attr("transform", function(d) { return "translate(" + (d.extra_w - (_w_value + w_gap + w_line + _radius - d.linestart)) + "," + (d.extra_y - dot_size/2) + ")"; })
                     .attr("width", dot_size)
                     .attr("height", dot_size)
                     // .style("fill","#999999");
                     .style("fill",dot_line_color)
                     // .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });


             // _bodyG.selectAll("g.chart-bars")
             //    // .selectAll(".chart-bars")
             //        .call(function(d,i){
             //            // var dd = d.selectAll("g");
             //            console.log(d);
             //        })
            // _line.forEach(function(d,i){

            //     var cur_category = d.category;
            //     var tag2 = "[class^='tag" + uid + cur_category.replace(_tag_replace, '')+"']";
            //     console.log(tag2);
            //     _bodyG.selectAll(tag2)
            //         // .filter( function(dd) {
            //         //     console.log(this.id);
            //         //     return (this.id == "tag" + uid + cur_category.replace(_tag_replace, ''))
            //         // })
            //         // .data(function(d,i){return [_line[i]]; })

            //        .call(function(d){
            //            // var dd = d.selectAll("g");
            //            console.log(this);
            //        })

            // })

        }

        // console.log(_radius, _radius/Math.SQRT2);
        // var xx = _radius/Math.SQRT2
        // // _bodyG
        // //     .append("path")
        // //     .attr("fill","darkred")
        // //     .attr("d", "M57,78A97,97 0 1,0 -57,-78L0,0Z")
        // //     .attr("transform", function(d, i) {
        // //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","#0086cd")
        //     .attr("d", "M"+xx + "," + xx + "l-30,30A" + _radius +"," + _radius + " 0 1,1 " +(-xx-30) + "," + (-xx+30) + "l30,-30L0,0Z")
        //     .attr("transform", function(d, i) {
        //         return "translate(431,132)"; })



        // var
        //     tt1 = "M5.939536975864663e-15,-97A97,97 0 1,1 -46.7301063878664,85.00174796425476L0,0Z",
        //     // tt1 = "M-46.7301063878664,85.00174796425476A97,97 0 0,1 -46.73010638786631,-85.0017479642548L0,0Z",
        //     // tt1 = "M-46.73010638786631,-85.0017479642548A97,97 0 0,1 6.833469578331816e-14,-97L0,0Z",

        //     // tt2 = "M"+ (barZoom + 5.939536975864663e-15)+",-97A97,97 0 1,1 -46.7301063878664,85.00174796425476L0,0Z",
        //     // tt3 = "M5.939536975864663e-15,-97
        //     //         l5,-5
        //     //         l-5.939536975864663e-15,+97
        //     //         l-46.7301063878664,85.00174796425476
        //     //         l-5,5
        //     //         L0,0Z",
        //     ttt = tt1.match(/^M(.+)\,(.+)A(.+)\,([^\s]+)\s.+\s(.+)\,(.+)L/),
        //     tt4 = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+ttt[1]+","+(-ttt[2])+"l"+ttt[5]+","+ttt[6]+"l"+(-barZoom)+","+barZoom+"L0,0Z",

        //     tt5 = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z";

        // console.log(tt1);
        // console.log(ttt[1], ttt[2], ttt[3], ttt[4], ttt[5], ttt[6]);
        // console.log(tt4);
        // console.log(tt5);

        // _bodyG
        //     .append("path")
        //     .attr("fill","black")
        //     .attr("d", tt1)
        //     .attr("transform", function(d, i) {
        //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","darkred")
        //     .attr("d", tt5)
        //     .attr("transform", function(d, i) {
        //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","red")
        //     .attr("d", tt1)
        //     .attr("transform", function(d, i) {
        //         return "translate("+ (431 + barZoom) +"," + (132 + (-barZoom)) + ")"; })

    }  // end of renderBars()


    function updatePies() {

        // pp = pie.
        // aa = arc()

        var bars =_bodyG.selectAll("g.chart-groups").selectAll("g.chart-bars")


        // // update bars to the d.val width
        // _bodyG.selectAll("g.chart-groups")
        //     .selectAll("g.chart-bars path")
        //     // .data(_data)
        //     .data(function(d){ return d.group})
        //     .transition().duration(g_duration)
        //     .attr({
        //         "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
        //         "fill": function(d) { return _data2colors[d.category]; },
        //         "d": function(d) { return barFun(_x(d.val))}
        //     });



        // update bars to the d.val values

        _pie.value(function(d) {return d.val; }); // change the value function

        _bodyG.selectAll("g.chart-groups")
            .selectAll("g.chart-bars path")
            // .data(_data)
            // .data(function(d){ return d.group})
            .data(function(d){return _pie(d.group)})
            .transition().duration(g_duration)
            .attrTween("d", arcTween)
            // .duration(g_duration/2)
            .attr({
            //     "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
                "fill": function(d) {return _data2colors[d.data.category]; }
            //     "d": function(d) { return barFun(_x(d.val))}
            });



        // update values position according to
        // newly calculated pie chart arcs
        // set Y position to centroids
        // and X to left of chart group
        // !!! All positions calculated from center of pie
        // !!! thats why we see negative X offset
        _bodyG.selectAll("g.chart-groups")
            // .selectAll("g.chart-bars text")
            .selectAll("text")
            // .append("text")
            .data(function(d){ return _pie(d.group)})
            // .attr("transform", function(d) {
            //     console.log("ddddd---------",d);
            //     d.outerRadius = _radius; // Set Outer Coordinate
            //     d.innerRadius = 0; // Set Inner Coordinate
            //     console.log("centroid:  ",_arc.centroid(d)[0],_arc.centroid(d)[1]);
            //     return "translate(" + _arc.centroid(d) + ")";
            // })
            .attr("y", function(d, i) {
                // console.log("centroid:  ",_arc2.centroid(d)[0],_arc2.centroid(d)[1]);
                return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) ;
            })
            .attr("x", function(d, i) {
                return -(_w_value + w_gap + w_line + _radius);
            })
            .attr("dy", ".35em")
            .attr("text-anchor", "start")
            // .text(function(d) {return d.data.val; })
            ;


        // update lines to initial values
        _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
             .data(function(d){ return _pie(d.group)})
             // .selectAll("line")
             // .transition().duration(g_duration)
             .attr("x1", function(d,i) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));})
             .attr("y1", function(d) { return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0);})
             .attr("x2", function(d) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));})
             .attr("y2", function(d) { return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0);})



        // // update lines according to the _data width
        _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
             .data(function(d){ return _pie(d.group)})
             // .selectAll("line")
             .transition().duration(g_duration)
             .attr("x2", function(d) { return _arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0)});



        // update dots on lines
        _bodyG.selectAll("g.chart-groups")
            .selectAll("rect")
            .data(function(d){ return _pie(d.group)})
            .attr("width", dot_size)
            .attr("height", dot_size)
            .attr("transform", function(d) { return "translate(" + (-(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length))) + "," + (_arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) - dot_size/2) + ")"; })
            .transition().duration(g_duration)
            .attr("transform", function(d) { return "translate(" + (_arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0)) + "," + (_arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) - dot_size/2) + ")"; });




        // update values
        _bodyG.selectAll("g.chart-groups")
            // .data(_data)
            // .data(function(d){console.log("d.group     ",d.group); return d.group})
            .selectAll("text")
            // .text(function(d){console.log("d.data     ",d.data); return d.data.val;})
            .transition().duration(g_duration)
            .tween("text", function(d) {
               // console.log(d);
                // get current value as starting point for tween animation
               var currentValue = +this.textContent;
               var ii = d3.interpolate(currentValue, d.data.val),
                   prec = (d.data.val + "").split("."),
                   round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                // console.log(d3.select(this).attr("class"));
                // d3.select(this).attr("class","chart-value_text");
                // console.log(d3.select(this).attr("class"));

                // this.attr( 'class', 'chart-value_text' );
               return function(t) {
                   // this.textContent = Math.round(ii(t));
                   // this.attr("class","chart-value_text");
                   this.textContent = local_num_format(Math.round(ii(t) * round) / round);
                   // d3.select(this).attr("font-size","10px");

               };
            });


        // update group names
        tmp_height=0;
        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)
            // .enter()
            // .transition().duration(g_duration/4)
            .attr("transform", function(d, i) {
                return "translate(" + ( (i)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups )) + "," + 0 +  ")";
            })
            .call(wrap, _pieWidth, 0, _group_names_TextWidth, _le, false);

        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)
            .call(textstyle);

        // // create array to link arc-line-dot-value
        // _bodyG.selectAll("g.chart-groups")
        //     // .data(_data)
        //     .selectAll("text")
        //     // .data(function(d){ return d.group})
        //     .data(function(d){ return _pie(d.group)})
        //     .call(place_values, 1, 2, 3);

    }  // end of updatePies()


    // Store the displayed angles in _current.
    // Then, interpolate from _current to the new angles.
    // During the transition, _current is updated in-place by d3.interpolate.
    function arcTween(a) {
      var i = d3.interpolate(this._current, a);
      this._current = i(0);
      return function(t) {
        return _arc(i(t));
      };
    }


    // function place_values(dataset, AA, BB, CC) {
    //   // txtWidth.length = 0;

    //   var vals=[];
    //   // console.log("place_value:   ",dataset);

    //   // var test = d3.select(this);
    //   // console.log("place_value:   ",test);

    //   // for(var ii = 0; ii < dataset.length; ii++) {
    //   //   // dataset.each(function(d,i) {
    //   //       vals[ii] = dataset[ii];
    //   //       vals[ii].forEach( function(d,i){
    //   //           // console.log(d.__data__,i,_arc2.centroid(d.__data__));
    //   //       })
    //   //       // vals[ii].push(d);
    //   //   // })
    //   // };

    //   dataset.each(function(d,i) {

    //     console.log("place_value:      ",d,i);

    //     // d.selectAll("g.chart-bars text")
    //     //     // .append("text")
    //     //     .text(function(dd){console.log("place_value:   ",dd,ii); return (dd)});


    //     vals.push(d);
    //     // vals.push({
    //     //     "index_base":i,
    //     //     "value": d.data.val,
    //     //     "x_txt": -(_w_value + w_gap + w_line + _radius),
    //     //     "y_txt": _arc2.centroid(d)[1],
    //     //     "x_line": _arc2.centroid(d)[0],
    //     //     "y_txt": _arc2.centroid(d)[1]
    //     // });



    //   //   var text = d3.select(this),
    //   //       // words = text.text().split(/[ \n]/).reverse(),
    //   //       words = text.text().split(/ /).reverse(),
    //   //       word,
    //   //       line = [],
    //   //       lineNumber = 0,
    //   //       lineHeight = 1.2, // ems
    //   //       y = text.attr("y"),
    //   //       dy = parseFloat(text.attr("dy")),
    //   //       ln = 0;


    //   //   var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
    //   //   var tmpMax = 0;
    //   //   while (word = words.pop()) {
    //   //       // console.log("_"+word+"_");
    //   //     line.push(word);
    //   //     tspan.text(line.join(" "));
    //   //     // console.log("_"+word+"_","---------------'",tspan.text(),"'", tspan.text().length);


    //   //     if (tspan.node().getComputedTextLength() > width) {
    //   //       line.pop();
    //   //       tspan.text(line.join(" "));
    //   //       line = [word];

    //   //       // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
    //   //       // that's why ((tmpMax>0)?++lineNumber:lineNumber)
    //   //       ln = (tmpMax>0)?++lineNumber:lineNumber;
    //   //       tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word);
    //   //     }
    //   //     // console.log(tspan.node().innerHTML);
    //   //     tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());

    //   //   }
    //   //   txtWidth.push(tmpMax);
    //   //   // console.log(txtWidth);
    //   //   //replace <\d+> in data texts to superscript
    //   //   doSuperscript(text);

    //   });
    // console.log("place_value:   ",vals);

    // }



    function renderLegend() {
    // Legend

        // add legend

        var legendContainer = _bodyG.selectAll("g.chart-legend")
            .data(_categoryMap)
            // .data(function(d){ return d.group})
            .enter()
            .append("g")
            // .attr("id", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })
            .attr("class", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })
            .classed('chart-legend', true)


        _bodyG.selectAll("g.chart-legend")
            .data(_categoryMap)
            .attr("visibility","hidden")  // turn legend off to make all calculations for word wrap
            .attr("transform", function(d,i) { return "translate(" +  (i*_legendSpace) + "," + (_height_chart + gap_to_legend) + ")"; })
            .on("click", function(d) {return onClick_Legend(d)});

        legendContainer
                .append("path")

        legendContainer
                .append("text")
                .attr("class", "chart-legend_text")


        _bodyG.selectAll("g.chart-legend path")
            // .data(_categoryMap)
            .attr({
                "fill": function(d) { return _data2colors[d.category]},
                "d": function(d) { return barFun(w_legend_bar)}
            })



        _bodyG.selectAll("g.chart-legend text.chart-legend_text")
            // .data(_categoryMap)
            // .append("text")
            .attr("x", function(d,i) { return (w_legend_bar + w_gap); })
            .attr("y", h_legend_text)
            // .attr("dy", ".35em")
            .attr("dy", "0")
            .text( function(d){ return d.category;})
            // .transition().delay(2000)
            .call(wrap, _legendSpace - w_legend_bar - w_gap - w_gap_between_legends, w_legend_bar + w_gap, _legendTextWidth, _le, true, _legendTextHeight);
            // .on("click", function(d){
            //     newOpacity = d.active ? 0.5 : 1;
            //     d3.selectAll("#tag"+d.category.replace(/\s+/g, ''))
            //         .transition().duration(100)
            //         .style("opacity", newOpacity);
            //     d.active = !d.active;
            //     });
        var tmp = _legendTextWidth.length;
        var w_add = (tmp > 1) ? ((_width - (tmp * (w_legend_bar + w_gap) + d3.sum(_legendTextWidth)) ) /(tmp - 1) - w_gap_between_legends) : 0;
        // w_add is correction of each legend width to fill all _width with legends
        // w_add = 0;
        var w_add2 = _width - d3.sum(_legendTextWidth) - (w_legend_bar + w_gap)*tmp - w_gap_between_legends * (tmp - 1);
        // w_add2 += 2;
        // console.log(w_add2);

        // _legendAlign base value is "left"
        if ( _legendAlign == "right" ) { w_add = 0; }
        if ( _legendAlign == "justify" ) { w_add2 = 0; }
        if ( _legendAlign == "left" ) { w_add = 0; w_add2 = 0; }



        _bodyG.selectAll("g.chart-legend")
            // .transition()
            .attr("transform",
                function(d,i) {
                    var tmp_w = 0, tmp_h = 0, tmp_max = 0;
                    var x_legend_line_number = 0, x_legend_item_width = 0;

                    for (var ii = 0; ii < i; ii++) {
                        tmp_w += w_legend_bar + w_gap +_legendTextWidth[ii] + w_gap_between_legends + w_add;
                    };

                    if (!(typeof _le === "undefined")) {
                      x_legend_line_number = Math.floor(i/_le.length);
                      x_legend_item_width = _le[i-(x_legend_line_number * _le.length)];
                      // console.log(i, x_legend_line_number, x_legend_item_width);

                      tmp_w = 0;
                      for (var ii = 0; ii < (i-(x_legend_line_number * _le.length)) ; ii++) {
                          tmp_w += _le[ii];
                      };

                      // console.log(i, x_legend_line_number, _le.length,Math.max.apply(Math,_legendTextHeight.slice(x_legend_line_number * _le.length,x_legend_line_number * _le.length + _le.length)));

                      if (x_legend_line_number>0) {
                        // get max number of lines in previous legend lines

                        for (var ii = 0; ii < x_legend_line_number ; ii++) {
                            tmp_max = Math.max.apply(Math,_legendTextHeight.slice((ii) * _le.length,(ii) * _le.length + _le.length));
                            tmp_h +=  ((tmp_max  * legend_line_height) + w_gap_between_legends);
                        };


                        // console.log(i, tmp_h, tmp_max);
                      } // else tmp_h = 0 because it is first line

                      // tmp_h = x_legend_line_number * (_legendTextHeight[i] * legend_line_height);
                      // console.log(tmp_w, tmp_h);
                      // cur_max_width = x_legend_item_width;
                    }

                    return "translate(" +  (w_add2 + tmp_w) + "," + (_height_chart + gap_to_legend + tmp_h) + ")";
                })

        _bodyG.selectAll("g.chart-legend")
            .attr("visibility","visible") // turn legend ON. All calculations for word wrap are completed.



    } // end of renderLegend()


    // function: wrap long lines
    // text - data array
    // width – max width of text
    // xposition – starting X position of text inside <tspan>
    // txtWidth – after function completion this array has width of each of calculated lines

    function wrap(text, width, xposition, txtWidth, widths, isLegend, txtHeight) {

      // _legendAlign = (typeof series.legend_align === "undefined") ? "left" : series.legend_align;
      if (isLegend && !(typeof widths === "undefined")) txtHeight.length = 0;
      txtWidth.length = 0;
      text.each(function(d,i) {
        var text = d3.select(this),
            // words = text.text().split(/[ \n]/).reverse(),
            words = text.text().split(/ /).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.2, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            ln = 0;
        var x_legend_line_number = 0,
            x_legend_item_width = 0,
            cur_max_width = 0;


        var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
        var tmpMax = 0;
        while (word = words.pop()) {
            // console.log("_"+word+"_");
          line.push(word);
          tspan.text(line.join(" "));
          // console.log("_"+word+"_","---------------'",tspan.text(),"'", tspan.text().length);


          cur_max_width = width;

          if (isLegend && !(typeof widths === "undefined")) {
            x_legend_line_number = Math.floor(i/widths.length);
            x_legend_item_width = widths[i-(x_legend_line_number * widths.length)];
            // console.log(i, x_legend_line_number, x_legend_item_width);

            cur_max_width = x_legend_item_width - (w_legend_bar + w_gap + w_gap_between_legends);
          }

          if (tspan.node().getComputedTextLength() > cur_max_width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];

            // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
            // that's why ((tmpMax>0)?++lineNumber:lineNumber)
            ln = (tmpMax>0)?++lineNumber:lineNumber;
            tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word);
          }
          // console.log(tspan.node().innerHTML);
          tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());

        }
        txtWidth.push(tmpMax);

        if (isLegend && !(typeof widths === "undefined")) {
            txtHeight[i] = Math.max((typeof txtHeight[i] === "undefined") ? 0 : txtHeight[i],lineNumber+1);
            // console.log(lineNumber, tspan.text(), txtHeight[i]);
        }
        // console.log(txtWidth);
        //replace <\d+> in data texts to superscript
        doSuperscript(text);

      });
    }

    function doSuperscript(text){
        //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
        // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
        //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
        if (text.html()){
            //replace <\d+> in data texts to superscript
            if(RegExp(/\&lt\;(\d+)\&gt\;/).test(text.html())) {
                var num = text.html().match(/\&lt\;(\d+)\&gt\;/)[1],
                    newtext = text.html().replace(/\&lt\;(\d+)\&gt\;/,'</tspan><tspan class="chart-sup" dy="-3">'+num+'</tspan><tspan>');
                // console.log(newtext);
                text.html(newtext);
            }
        } else {console.log("!!! https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");}
    }

    function textstyle(dd){

        dd.each(function(d,i) {
            var text = d3.select(this);

            //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
            // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
            //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
            if (text.html()){
                //replace <*> in data texts to blue / slash
                var newtext = text.html().replace(/\&lt\;\*\&gt\;/,'</tspan><tspan class="chart-blue">/</tspan><tspan>');
                text.html(newtext);
            } else {
                console.log("!!!https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");
                    // !!!!!!!!! https://code.google.com/p/innersvg/   needed here everywhere except chrome
            }

        })
    }

    function onClick_Legend(d){

        clearTimeout(_timerId);

        // Determine if current line is visible

        var newOpacity = 1,
            check_active = d.active;

        if (check_active) {
            _categoryMap.forEach( function(dd){ check_active = check_active && dd.active;} )
            newOpacity = check_active ? fadeLevel : 1;

        } else {
            newOpacity = fadeLevel;
            check_active = true;
            d.active = true;

        }

        // Hide or show the elements based on the CLASS

        // set opacity of other to newOpacity
        var cur_category = d.category;

        _bodyG.selectAll("[class^='tag']")
            // .filter( function(dd) {
            //     return (this.id == "tag" + uid + cur_category.replace(_tag_replace, '')) ? false : true
            // })
            .transition().duration(100)
            .style("opacity", newOpacity);

         // set opacity of shadow to newOpacity
        _bodyG.selectAll(".big_shadow")
            .transition().duration(100)
            .style("opacity", newOpacity);

        // set opacity of selected to 1
        _bodyG.selectAll(".tag" + uid + cur_category.replace(_tag_replace, ''))
            .transition().duration(100)
            .style("opacity", 1);

        // Update whether or not the elements are active
        var cur_active = d.active;
        _categoryMap.forEach( function(dd){ dd.active = !check_active;} )
        d.active = cur_active;
        // console.log(d);

        _timerId = setTimeout(function() {
            _bodyG.selectAll("[class^='tag']")
            .transition().duration(g_duration)
            .style("opacity", 1);

             // set opacity of shadow to newOpacity
            _bodyG.selectAll(".big_shadow")
            .transition().duration(g_duration)
            .style("opacity", 1);

            _categoryMap.forEach( function(dd){ dd.active = true;} )
        }, Time2KeepCategoryActivated);
    }

    function onMouseOver_Bar(d) {

        var pnode = d3.select(d.parentNode.parentNode),
            chart_num = pnode.attr("class").match(/\bc\_\_.{4}\_(\d+)/)[1],
            zoomContainer = d3.select(d.parentNode.parentNode).selectAll(".chart-groups-zoom"),
            pathContainer = d3.select(d.parentNode),
            cur_cat = pathContainer[0][0].__data__.data.category,
            cur_val = pathContainer[0][0].__data__.data.val,
            isactive = false,

            selected_class = "."+pathContainer.attr("class").match(/\bnum.{4}\_\d+/g)[0],
            selected_class_extra = "."+pathContainer.attr("class").match(/\bnum.{4}\_\d+/g)[0],

            s_path = pathContainer.selectAll("path"),
            s_path_fill = s_path.attr("fill"),
            s_path_darker = ColorLuminance(s_path_fill, -0.5),

            // get path description "d"
            s_path_data = s_path.attr("d"),
            // console.log(s_path_data);
            // ttt =  get parts of s_path_data with regex
            // ttt = s_path_data.match(/^M(.+)\,(.+)A(.+)\,([^\s]+)\s.+\s(.+)\,(.+)L/);
            ttt = s_path_data.match(/[MmZzLlHhVvCcSsQqTtAa]|[-+]?[0-9]*\.?[0-9]+(?:[eE][-+]?[0-9]+)?/gi);

            // console.log(_line[chart_num]);
            // console.log(pathContainer.attr("class"),pathContainer.attr("class").match(/\bnum.{4}\_\d+/g));
            // console.log(selected_class);
            // console.log(selected_class_extra);

            // ttt.forEach(function(d,i){console.log(i, ":     ", d);});

            // var pathSegmentPattern = /[a-z][^a-z]*/ig;
            // var pathSegments = s_path_data.match(pathSegmentPattern);
            // svg_COMMANDS = 'MmZzLlHhVvCcSsQqTtAa';

            // console.log(pathSegments);

            var side_shadow_path_data_start = "M"+ttt[1]+","+ttt[2]+"l"+0+","+(-0)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z",
            side_shadow_path_data = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z",
            arc_sweep = ttt[8]^ttt[8],
            side_shadow_arc_data_start = "M"+ttt[1]+","+ttt[2]+"A"+ttt[4]+","+ttt[5]+" "+ttt[6]+" "+ttt[7]+","+ttt[8]+" "+ttt[9]+","+ttt[10]+"l"+0+","+(-0)+"A"+ttt[4]+","+ttt[5]+" "+ttt[6]+" "+ttt[7]+","+arc_sweep+" "+ttt[1]+","+ttt[2]+"l0,0Z";
            side_shadow_arc_data =       "M"+ttt[1]+","+ttt[2]+"A"+ttt[4]+","+ttt[5]+" "+ttt[6]+" "+ttt[7]+","+ttt[8]+" "+ttt[9]+","+ttt[10]+"l"+barZoom+","+(-barZoom)+"A"+ttt[4]+","+ttt[5]+" "+ttt[6]+" "+ttt[7]+","+arc_sweep+" "+(parseFloat(ttt[1])+barZoom)+","+(parseFloat(ttt[2])-barZoom)+"l"+(-barZoom)+","+barZoom+"Z";

        // console.log(s_path_data);
        // console.log(side_shadow_arc_data_start);
        // console.log(side_shadow_arc_data);

        // d3.selectAll("line"+selected_class)
        // .attr("display","none");

        // zoomContainer
        //     .text("AAAAAAAAAAA")


        // console.log(zoomContainer);


        onMouseOver_Tolltip(cur_val);


        // parent_pathContainer
        //     .selectAll(".chart-groups")
        //     .append("g")
        //     .attr("class", "AAAAAAAAAAAAAAAAAAAAA")

        _categoryMap.forEach( function(el) {
            if (el.category===cur_cat &&  el.active==true) isactive = true;
        })

        // console.log(pathContainer, cur_cat, cur_val);
        // if this Legend category ic active (not faded)
        if (isactive) {


            // var selected_class =   "num" + uid + "_" + d.data.num;
            // pathContainer
            d3.selectAll("line"+selected_class)
                // .select("line")
                .transition()
                .attr("x1", function(d) { return _arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0);})
                // .attr("x2", function(d) { return _w_group_name + w_gap + _x(d.val) + w_gap;})
                .transition()
                .attr("x1", function(d,i) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));});

                // .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)});

            // add shadow path same color as arc
            zoomContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .attr({
                    // "id": "chart-zoombar_shadow",
                    "class": "chart-zoombar_shadow"
                    // "fill": "#0086cd"
                })
                .attr("fill", s_path_fill)
                .attr("d", s_path_data)

            // add side shadow
            zoomContainer
                .append("path")
                .classed("chart-zoombar_shadow_dark",true)
                .attr("fill", s_path_darker)
                .attr("d", side_shadow_path_data_start)

            // add side arc shadow
            zoomContainer
                .append("path")
                .classed("chart-zoombar_shadow_arc_dark",true)
                .attr("fill", s_path_darker)
                .attr("d", side_shadow_arc_data_start)

            // add zoombar same color as arc
            zoomContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .classed("chart-zoombar",true)
                .attr("fill", s_path_fill)
                .attr("d", s_path_data)




            // color shadow darker
            zoomContainer
                .select("path.chart-zoombar_shadow")
                .attr("fill", s_path_darker)

            // pnode.selectAll("path.dot_path")
            //     .transition().duration(g_duration/4)
            //     .attr("transform", "translate(" + barZoom + "," + (-barZoom) + ")")

            // pnode.selectAll("path.dot_path")
            pnode.selectAll(".extra")
                // .transition().duration(g_duration/4)
                // .style("opacity", 0)
                .style("visibility","hidden")

            zoomContainer
                .select("path.chart-zoombar")
                .transition().duration(g_duration/4)
                .attr("transform", "translate(" + barZoom + "," + (-barZoom) + ")")

            // update side shadow path with new "d"
            zoomContainer
                .select("path.chart-zoombar_shadow_dark")
                .transition().duration(g_duration/4)
                .attr("d", side_shadow_path_data)

            // update side shadow arc with new "d"
            zoomContainer
                .select("path.chart-zoombar_shadow_arc_dark")
                .transition().duration(g_duration/4)
                .attr("d", side_shadow_arc_data)

            // make selected value bold
            d3.selectAll("text"+selected_class)
                // .selectAll("text")
                .classed("chart-selected_value", true);


        }
    }

    function onMouseOut_Bar(d) {


        onMouseOut_Tolltip();

        var pnode = d3.select(d.parentNode.parentNode),
            zoomContainer = d3.select(d.parentNode.parentNode).selectAll(".chart-groups-zoom"),
            pathContainer = d3.select(d.parentNode);

        var selected_class = "."+pathContainer.attr("class").match(/\bnum.{4}\_\d+/g)[0];

        zoomContainer
            .selectAll(".chart-zoombar")
            // .transition().duration(50)
            .attr("transform", "translate(" + (0) + "," + (0) + ")")
            .remove();
        zoomContainer
            .selectAll(".chart-zoombar_shadow_dark")
            .remove();
        zoomContainer
            .selectAll(".chart-zoombar_shadow_arc_dark")
            .remove();
        zoomContainer
            .selectAll(".chart-zoombar_shadow")
            .remove();
        d3.selectAll("text"+selected_class)
            // .selectAll("text")
            .classed("chart-selected_value", false);
        pnode.selectAll(".extra")
                // .transition().duration(g_duration/4)
            // .style("opacity", 1)
            .style("visibility","visible")

        // pnode.selectAll("path.dot_path")
        //         // .transition().duration(g_duration/4)
        //     .attr("transform", "translate(" + (0) + "," + (0) + ")")


        // var thisBar = d3.select(d);
        // // console.log(thisBar);

        // thisBar
        // .transition().duration(g_duration/4)
        // .attr({
        //     "d": function(d) { return barFun(_x(d.val))},
        //     "fill": function(d) { return _data2colors[d.category]; }
        // })
    }

    function onMouseMove_Bar(d) {

        onMouseMove_Tolltip();

    }

    function onMouseOver_Tolltip(value) {if (_tooltipOn) return _tooltip.text(value).style("visibility", "visible");}
    function onMouseMove_Tolltip() {if (_tooltipOn) return _tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");}
    function onMouseOut_Tolltip() {if (_tooltipOn) return _tooltip.style("visibility", "hidden");}



    _chart.setSeries(cdata);
    _chart.render();
    return _chart;
}



// @codekit-append "cmain-append.js";

