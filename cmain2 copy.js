// @codekit-prepend "cdata3.js";

var all_colors = ["#21a7d6","#999999","#0086cd","#78bfe2", "#b1d8ed", "#cdcdcd", "#a2bba8", "#becec0","#dee6df","#0086cd","#8c7dae", "#cbc4de", "#efad63", "#f4c58f"];

var c01_0222_data = {
    title: "XXXXXXX Отраслевая структура кредитного портфеля банка развития, %",
    sub_title: "Факт на 01.01.2014",

    w_group_name: 70,
    legendHeight: 100,
    tooltip : false,
    legend_align: "left",
    legend_row: 4,
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2", "#b1d8ed", "#cdcdcd", "#a2bba8", "#becec0","#dee6df","#0086cd","#8c7dae", "#cbc4de", "#efad63", "#f4c58f"],
    data: [
        { "group_name":"Факт на 01.01.2014",
            "group": [
            {"val": 40.2, "category": "Отрасли промышленности","offsetY":30,"offsetX":-50},
            {"val": 25.0, "category": "Секторы инфраструктуры","offsetY":15},
            {"val": 9.1, "category": "Агропромышленный комплекс","offsetY":10},
            {"val": 2.3, "category": "Прочие отрасли"},
            {"val": 10.3, "category": "Авиастроение и авиция, за исключением вертолетостроения"},
            {"val": 0.1, "category": "Судостроение"},
            {"val": 13.0, "category": "Строительство и сопутствующие отрасли"}
            ]
        },

        { "group_name":"Стратегия (ориентир на\xA02015 год)",
            "group": [
            {"val": 40, "category": "Отрасли промышленности","offsetY":0,"offsetX":0},
            {"val": 50, "category": "Секторы инфраструктуры","offsetY":0},
            {"val": 10, "category": "Агропромышленный комплекс","offsetY":-2}
            ]
        }
    ]
};

var c01_02_data = {
    title: "Отраслевая структура кредитного портфеля банка развития, %",
    sub_title: "Факт на 01.01.2014",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"Факт на 01.01.2014",
            "group": [
            {"val": 49.2, "category": "Отрасли промышленности","offsetY":30,"offsetX":-50},
            {"val": 39.4, "category": "Секторы инфраструктуры","offsetY":15},
            {"val": 9.1, "category": "Агропромышленный комплекс","offsetY":10},
            {"val": 2.3, "category": "Прочие отрасли"}
            ]
        },

        { "group_name":"Стратегия (ориентир на\xA02015 год)",
            "group": [
            {"val": 40, "category": "Отрасли промышленности","offsetY":0,"offsetX":0},
            {"val": 50, "category": "Секторы инфраструктуры","offsetY":0},
            {"val": 10, "category": "Агропромышленный комплекс","offsetY":-2}
            ]
        }
    ]
};

var c01_02_data__ = {
    title: "Отраслевая структура кредитного портфеля банка развития, %",
    sub_title: "Факт на 01.01.2014",

    w_group_name: 70,
    legendHeight: 30,
    tooltip : false,
    legend_align: "left",
    colors: ["#21a7d6","#999999","#0086cd","#78bfe2"],
    data: [
        { "group_name":"Факт на 01.01.2014",
            "group": [
            {"val": 49.2, "category": "Отрасли промышленности","offsetY":30,"offsetX":-50},
            {"val": 39.4, "category": "Секторы инфраструктуры","offsetY":15},
            {"val": 9.1, "category": "Агропромышленный комплекс","offsetY":10},
            {"val": 2.3, "category": "Прочие отрасли"}
            ]
        },

        { "group_name":"Стратегия (ориентир на\xA02015 год)",
            "group": [
            {"val": 58.0, "category": "Отрасли промышленности","offsetY":25,"offsetX":-90},
            {"val": 34.0, "category": "Секторы инфраструктуры","offsetY":27},
            {"val": 8.0, "category": "Агропромышленный комплекс","offsetY":-2}
            ]
        }
    ]
};
/*=================================
=          Global Functions       =
=================================*/


function randomData() {
    return Math.random() * 9;
}

/*-----  End of Global Functions  ------*/

function pieChart(place,cdata,w) {

    var uid = generateUID(),

        base_place = "body",

        chart_fullwidth = 538,
        barHeight = 18,
        barZoom = 5, // zoom distance
        _bs = 20, // size of BID shadow under pie chart
        barHeightBig = barHeight+barZoom,
        skos = barHeight/2,
        // bigskos = barHeightBig/2.2,  
        // placefortext = 30,
        // line_width = 50,
        mtop = 50,
        mbottom = 50,
        // dist2line = 30,
        // start_text = 50,
        // bar_width = fullwidth - start_text - dist2line - line_width - placefortext,
        dot_line_color = "#575757",
        dot_size = 2,
        stroke_width = 0.7,
        gap_between_bars = 0,
        gap_between_groups = 10,
        gap_to_legend = 30,

        h_legend_text = 10,

        g_duration = 1000,

        w_line = 50,
        w_value_one_character = 5,  // width of one character in value field 
        //total width of field = w_value + w_value_one_character * d.val length
        w_gap = 10,
        w_gap_between_legends = 3*w_gap,
        w_bar_start_width = 10,   // minimum width of bar to start growth animation
        w_legend_bar = 22;

    var _pieWidth = w,  // set initial width og pie chart to w
        _radius = 1,
        _arc,  _arc2,  // set in renderBody()
        _arc_radius_modifier = 0.7,
        _pie;  // set in renderBody()

    var fadeLevel = 0.2, // level of fading of charts
        Time2KeepCategoryActivated = 5000;  // 4 sec keep legent category activated after click and activate all after that

        var ru_RU = {
          "decimal": ",",
          "thousands": "\xa0",
          "grouping": [3],
          "currency": ["", " руб."],
          "dateTime": "%A, %e %B %Y г. %X",
          "date": "%d.%m.%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
          "shortDays": ["вс", "пн", "вт", "ср", "чт", "пт", "сб"],
          "months": ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
          "shortMonths": ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
        };
        var en_US ={
          "decimal": ".",
          "thousands": ",",
          "grouping": [3],
          "currency": ["$", ""],
          "dateTime": "%a %b %e %X %Y",
          "date": "%m/%d/%Y",
          "time": "%H:%M:%S",
          "periods": ["AM", "PM"],
          "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
          "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        };

        var d3RU = d3.locale(ru_RU),
            d3EN = d3.locale(en_US),
            local_num_format = d3RU.numberFormat();
            // local_num_format = d3EN.numberFormat();

    /*-----  End of Globals  ------*/



    w = (typeof w === "undefined") ? chart_fullwidth : w;   // if no w set, default value is chart_fullwidth

    var _chart = {},

        _width = w, // to change width of chart dynamicaly we have to pass w to function
        _height = 300,  // full height chart+legend (start with free value 300px)
        // _margins = {top: 0, left: 0, right: 0, bottom: 0},
        _x, _y,
        _data = [],
        _data2colors = [],
        // _colors = d3.scale.category10(),
        _svg,
        _bodyG,

        _max_data_value = 0,

        _groups_total = 0,
        _bars_total = 0,
        _height_chart = 0,  // height of chart part without legend
        _legendSpace = 0,  // horizontal width of one legend block
        _legendHeight = barHeight,  // starting value for height of legend block
        _legendLineHeight = barHeight * 0.2,  // height of one legend line
        _legendAlign = "left",

        _legendTextWidth = [],
        _legendTextHeight = [],
        _group_names_TextWidth = [],
        _group_names_TextHeight = [],

        _categoryNames = [],  // list of unique data.group.categories
        _categoryMap = [],  // list on category objects for chart legend: {category,active}

        _arcs = [],

        _tooltip,
        _timerId,
        _w_group_name,
        _w_bar,
        _w_value = 5;  // base width of value field;

    var _tag_replace = /[|&;$%@"<>()+,. ]/g;  // regex to replace invalid characters in selector name




    var barFun = function(x) { 
        return ["m",[skos,0],
        "l",[x-skos,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[-x+skos,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],       
        "Z"].join(" ");
        }

    var barFunLeft = function(x) { 
        return ["m",[skos,0],
        "l",[0,0],
        "l",[0,skos],
        "l",[-skos,barHeight-skos],
        "l",[0,0],
        "l",[0,-(barHeight-skos)],
        "l",[skos,-skos],       
        "Z"].join(" ");
        }

    function generateUID() {
        return ("0000" + (Math.random()*Math.pow(36,4) << 0).toString(36)).slice(-4)
    }

    function ColorLuminance(hex, lum) {

        // validate hex string
        hex = String(hex).replace(/[^0-9a-f]/gi, '');
        if (hex.length < 6) {
            hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
        }
        lum = lum || 0;

        // convert to decimal and change luminosity
        var rgb = "#", c, i;
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i*2,2), 16);
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
            rgb += ("00"+c).substr(c.length);
        }

        return rgb;
    }


    function quadrantWidth() {
        // return _width - _margins.left - _margins.right;
        return _width;

    }

    function quadrantHeight() {
        // return _height - _margins.top - _margins.bottom;
        return _height;

    }


    //.setSeries – set data and prepare corresponding vars
    _chart.setSeries = function (series) {
        _data = series.data;
        // _data2colors = colors;
        _legendHeight = series.legendHeight;
        _legendAlign = (typeof series.legend_align === "undefined") ? "left" : series.legend_align;

        _w_group_name = series.w_group_name;
        // _w_bar = _width - ( _w_group_name + w_gap + w_gap + w_line + w_gap + _w_value);



        _groups_total = _data.length;  // get number of pie charts

        _max_data_value = 0;
        // calculate max value of ALL val
        _max_data_value = d3.max(_data, 
        function(d) {
            
            return d3.max(d.group, function(d) { 
                // var  prec = (d.val + "").split("."),
                //      round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;

                // console.log(d.val, (d.val + "").length, prec, round);
                return parseFloat(d.val); })
        });

        _bars_total = 0;
        _data.forEach(function(d,i) {           
        //     // calculate total number of bars
            d.group.forEach(function(dd,ii){
                // store current number to NUM property of each data element
                dd.num = _bars_total++;
            })
        })


        // add initial values for arc tween animation
        // set all values to 0 and the last value to any nonzero
        // so we will see how pie sectors grow from initial to real values
        // pushing last sector from 100% to its real value
        _data.forEach(function(d,i) {
            var last = 0;           
        // add initial values for arc tween animation
            d.group.forEach(function(dd,ii){
                dd.initial = 0;
                last = ii;
            })
            d.group[last].initial = 1;  // set lat value to huge to fill the whole pie
        })
        // console.log(_data);

        /*==========  prepare legend  ==========*/

        // clear legend arrays
        _categoryNames.length = 0;
        _categoryMap.length = 0;

        //create list of unique data.group.categories 

        // data2.forEach(group.forEach( function(d) {categoryNames[d.category] = d;});
        _data.forEach( function(d){ 
            d.group.forEach( function(dd){_categoryNames.push(dd.category);} )  // collect all category names
        });
        _categoryNames = d3.set(_categoryNames).values();  // keep unique names only

        var tmpcolors = [];

        // for every unique category create object
        // to control if category of legend is active or not
        for (var ii = 0; ii < _categoryNames.length; ii++) {
            _categoryMap.push({
                category: _categoryNames[ii],
                active: true
            })

            // set colors for each category
            // set RED color for all categories with no color specified
            _data2colors[_categoryNames[ii]] = (ii<series.colors.length) ? series.colors[ii] : "red"; 
            
        }


        // _legendSpace = ( _width - (w_gap-1)*_categoryMap.length ) / _categoryMap.length;

        _legendSpace = ( _width - (w_gap-1)*_categoryMap.length ) / ((typeof series.legend_row === "undefined") ? _categoryMap.length : series.legend_row); 
        // console.log(series.legend_row,_legendSpace);


        /*==========  set widths  ==========*/

        _pieWidth = ( w - gap_between_groups*(_groups_total-1) ) / _groups_total;

        _radius = (_pieWidth - _w_value - w_gap - w_line - barZoom) / 2;
        // _w_group_name + w_gap + w_gap + w_line + w_gap + _w_value


        /*==========  set heights  ==========*/

        // height of chart part
        // _height_chart = barZoom + barHeight * _bars_total + gap_between_bars * (_bars_total - 1) + gap_between_groups * (_groups_total - 1);
        _height_chart = _pieWidth;



        // total height of svg: height of chart + height of legend block
        _height = _height_chart + gap_to_legend + _legendHeight;  // add place for legend

        // console.log(_height_chart);
        _tooltipOn = series.tooltip;

        return _chart;
    };


    // .render – render chart
    _chart.render = function () {

        if (!_data.length) {console.log("ERROR: Set data with .setSeries before running .render"); return false;}
        if (!_svg) {  _svg = d3.select(base_place).select(place).append("svg");}
        if (!_tooltip) {  _tooltip = d3.select(base_place).select(place).append("div");}

        _svg
            // .transition().duration(200)
            .attr("height", _height)
            .attr("width", _width);

        _x = d3.scale.linear()
            .domain([0, _max_data_value])
            .range([w_bar_start_width, _w_bar]);


        // renderAxes(_svg);

        defineBodyClip(_svg);
        renderBody(_svg);
    };

    function defineBodyClip(svg) {
        var padding = 5,
            bodyclipplace = place+"_body-clip";

        svg.select("defs")
        .remove("defs");

        svg.append("defs")
                .append("clipPath")
                .attr("id", bodyclipplace)
                .append("rect")
                .attr("x", 0)
                .attr("y", 0)
                .attr("width", quadrantWidth() + 2 * padding)
                .attr("height", quadrantHeight());
    }

    function renderBody(svg) {
        var bodyclipplace = place+"_body-clip";
        if (!_bodyG)
            _bodyG = svg.append("g")
                    // .attr("class", "body")
                    // .attr("transform", "translate(" 
                    //         + xStart() 
                    //         + "," 
                    //         + yEnd() + ")")
                    .attr("clip-path", "url("+bodyclipplace+")");

        _arc = d3.svg.arc()
            .outerRadius(_radius)
            .innerRadius(0);

        _arc2 = d3.svg.arc()
            .outerRadius(_radius)
            .innerRadius(_radius*_arc_radius_modifier);

        _pie = d3.layout.pie()
            .sort(null)
            // .value(function(d) { return d.val; });
            .value(function(d) { return d.initial; });


        renderPies();
        renderLegend();
        renderTooltip();
    }
    
    function renderTooltip(){
        _tooltip 
            .classed("chart-tooltip",true)
            .style("visibility", "hidden");
    }



    


    function renderPies() {
        // var padding = 2; // <-A
        // var tmp_height = barZoom;   // move first bar down for zoomBar to fit it zoomed version
        var tmp_height = 0;


        // add groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            .enter()
            .append("g")
            .attr('class', 'chart-groups');
        // update groups
        _bodyG.selectAll("g.chart-groups")
            .data(_data)
            // .transition()
            .attr("transform", function(d, i) { 
                return "translate(" + ( (i+1)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups )) + "," + _pieWidth/2 +  ")";
            });


        // add big shadow
        var xx = _radius/Math.SQRT2;
        _bodyG.selectAll("g.chart-groups")
            .append("path")
            .classed("big_shadow",true)
            .attr("fill","#0086cd")
            .attr("d", "M"+xx+","+xx+"l"+(-_bs)+","+_bs+"A"+_radius+","+_radius+" 0 1,1 " +(-xx-_bs)+","+(-xx+_bs)+"l"+_bs+","+(-_bs)+"L0,0Z")


        
        // add bars
        var barContainer = _bodyG.selectAll("g.chart-groups")    
            .selectAll("g.chart-bars")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("g");

        
        var j = 0, jj = 0;
        barContainer
            // .attr("id", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, ''); })
            .attr("class", function(d,i,ii) {
                
                // console.log(i,ii);
                // if (!_arcs[ii]) {_arcs[ii] = []}
                // _arcs[ii].push({

                //     "num":j,
                //     "classname":"tag" + uid + d.data.category.replace(_tag_replace, ''),
                //     "g_arc":d,
                //     "arc":null

                // });
                // j++;

                // if (!_arcs[ii]) {_arcs[ii] = []}
                _arcs.push({

                    "num":j,
                    "classname":"tag" + uid + d.data.category.replace(_tag_replace, ''),
                    "g_arc":d,
                    "arc":null

                });
                j++;

                return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num;
            })
            .classed('chart-bars', true)
            // .attr("transform", function(d, i) { return "translate(0," + (i * barHeight + gap_between_bars) +  ")"; })

        //draw bars with starting w_bar_start_width --- as set by DOMAIN/RANGE: _x(0) = w_bar_start_width 
            .append("path")
            .attr('class',function(d,i,ii) {
                 return"chart-paths num" + uid + "_" + d.data.num;
            })
            .attr("d", _arc)
            .attr({
                // "transform": "translate(" + (_w_group_name + w_gap) + ",0)",

                // set real color
                // "fill": function(d) { return _data2colors[d.data.category]; }

                // set color to white. color real value during update
                "fill": function(d) { return "white"; }  

                // "d": function(d) { return _arc;}
            })
            .each(function(d) {
                // _arcs[ii][i].arc = d;
                _arcs[jj++].arc = d;

                // console.log(ii,i,_arcs); 
                this._current = d;
            })
            .on("mouseover", function(d) {return onMouseOver_Bar(this)})
            .on("mousemove", function(d) {return onMouseMove_Bar(this)})
            .on("mouseout", function(d) {return onMouseOut_Bar(this)});


        // add group for zoom elements
        _bodyG.selectAll("g.chart-groups")
            .append("g")
            .attr('class', 'chart-groups-zoom');


        // add values
        // set values to initial zero
         _bodyG.selectAll("g.chart-groups")
            .selectAll("text")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("text")
                // .attr("x", function(d) { return x(d.val) + 50; })
                // .attr("x", function(d, i) { 
                //     return -((i+1)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups ));
                // })
                // .attr("y", function(d, i) { 
                //     return w_gap - _radius;
                // })
                .attr("dy", ".35em")
                .attr("text-anchor","start")
                .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; })
                .classed('chart-value_text',true)
                .text(function(d) { return 0; });


        // add dots on lines
         _bodyG.selectAll("g.chart-groups")
            .selectAll("rect")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("rect")
                 // .attr("transform", function(d) { return "translate(" + (_w_group_name + w_gap + _x(w_bar_start_width) + w_gap) + "," + (barHeight - dot_size)/2 + ")"; })
                 // .attr("width", dot_size)
                 // .attr("height", dot_size)
                 // .style("fill","#999999");
                 .style("fill",dot_line_color)
                 .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });

        // add lines with starting length  according to starting w_bar_start_width 
         _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
            .data(function(d){ return _pie(d.group)})
            .enter()
            .append("line")
                // .transition().duration(2000) 
                // .attr("x1", function(d) { return _w_group_name + w_gap + _x(w_bar_start_width) + w_gap;})
                // .attr("y1", barHeight/2)
                // .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)})
                // .attr("y2", barHeight/2)
                .attr("stroke-width", stroke_width)
                // .attr("stroke", "#999999");
                .attr("stroke", dot_line_color)
                .attr("class", function(d) { return "tag" + uid + d.data.category.replace(_tag_replace, '') + " num" + uid + "_" + d.data.num; });


        // add group names  
        _bodyG.selectAll("text.chart-group_text")
            .data(_data)
            .enter()
            .append("text")
            .attr("x", 0)
            .attr("y", w_gap)
            .attr("dy", ".35em")
            .attr('class', 'chart-group_text')
            .attr("transform", function(d, i) { 
                return "translate(0,0)"; })
            .text(function(d) { return d.group_name});


        updatePies();


        // console.log(_radius, _radius/Math.SQRT2);
        // var xx = _radius/Math.SQRT2
        // // _bodyG
        // //     .append("path")
        // //     .attr("fill","darkred")
        // //     .attr("d", "M57,78A97,97 0 1,0 -57,-78L0,0Z")
        // //     .attr("transform", function(d, i) { 
        // //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","#0086cd")
        //     .attr("d", "M"+xx + "," + xx + "l-30,30A" + _radius +"," + _radius + " 0 1,1 " +(-xx-30) + "," + (-xx+30) + "l30,-30L0,0Z")
        //     .attr("transform", function(d, i) { 
        //         return "translate(431,132)"; })



        // var 
        //     tt1 = "M5.939536975864663e-15,-97A97,97 0 1,1 -46.7301063878664,85.00174796425476L0,0Z",
        //     // tt1 = "M-46.7301063878664,85.00174796425476A97,97 0 0,1 -46.73010638786631,-85.0017479642548L0,0Z",
        //     // tt1 = "M-46.73010638786631,-85.0017479642548A97,97 0 0,1 6.833469578331816e-14,-97L0,0Z",
            
        //     // tt2 = "M"+ (barZoom + 5.939536975864663e-15)+",-97A97,97 0 1,1 -46.7301063878664,85.00174796425476L0,0Z",
        //     // tt3 = "M5.939536975864663e-15,-97
        //     //         l5,-5
        //     //         l-5.939536975864663e-15,+97
        //     //         l-46.7301063878664,85.00174796425476
        //     //         l-5,5
        //     //         L0,0Z",
        //     ttt = tt1.match(/^M(.+)\,(.+)A(.+)\,([^\s]+)\s.+\s(.+)\,(.+)L/),
        //     tt4 = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+ttt[1]+","+(-ttt[2])+"l"+ttt[5]+","+ttt[6]+"l"+(-barZoom)+","+barZoom+"L0,0Z",

        //     tt5 = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z";
        
        // console.log(tt1);
        // console.log(ttt[1], ttt[2], ttt[3], ttt[4], ttt[5], ttt[6]);
        // console.log(tt4);
        // console.log(tt5);

        // _bodyG
        //     .append("path")
        //     .attr("fill","black")
        //     .attr("d", tt1)
        //     .attr("transform", function(d, i) { 
        //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","darkred")
        //     .attr("d", tt5)
        //     .attr("transform", function(d, i) { 
        //         return "translate(431,132)"; })
        // _bodyG
        //     .append("path")
        //     .attr("fill","red")
        //     .attr("d", tt1)
        //     .attr("transform", function(d, i) { 
        //         return "translate("+ (431 + barZoom) +"," + (132 + (-barZoom)) + ")"; })

    }  // end of renderBars()


    function updatePies() {

        // pp = pie.
        // aa = arc()

        var bars =_bodyG.selectAll("g.chart-groups").selectAll("g.chart-bars")


        // // update bars to the d.val width 
        // _bodyG.selectAll("g.chart-groups")
        //     .selectAll("g.chart-bars path")
        //     // .data(_data)
        //     .data(function(d){ return d.group})   
        //     .transition().duration(g_duration)
        //     .attr({
        //         "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
        //         "fill": function(d) { return _data2colors[d.category]; },
        //         "d": function(d) { return barFun(_x(d.val))}
        //     });
        


        // update bars to the d.val values

        _pie.value(function(d) {return d.val; }); // change the value function

        _bodyG.selectAll("g.chart-groups")
            .selectAll("g.chart-bars path")
            // .data(_data)
            // .data(function(d){ return d.group})   
            .data(function(d){return _pie(d.group)})
            .transition().duration(g_duration)
            .attrTween("d", arcTween)
            // .duration(g_duration/2)
            .attr({
            //     "transform": "translate(" + (_w_group_name + w_gap) + ",0)",
                "fill": function(d) {return _data2colors[d.data.category]; }
            //     "d": function(d) { return barFun(_x(d.val))}
            });


                
        // update values position according to
        // newly calculated pie chart arcs
        // set Y position to centroids
        // and X to left of chart group
        // !!! All positions calculated from center of pie
        // !!! thats why we see newgative X offset
        _bodyG.selectAll("g.chart-groups")
            // .selectAll("g.chart-bars text")
            .selectAll("text")
            // .append("text")
            .data(function(d){ return _pie(d.group)})
            // .attr("transform", function(d) {
            //     console.log("ddddd---------",d);
            //     d.outerRadius = _radius; // Set Outer Coordinate
            //     d.innerRadius = 0; // Set Inner Coordinate
            //     console.log("centroid:  ",_arc.centroid(d)[0],_arc.centroid(d)[1]);
            //     return "translate(" + _arc.centroid(d) + ")";
            // })
            .attr("y", function(d, i) { 
                // console.log("centroid:  ",_arc2.centroid(d)[0],_arc2.centroid(d)[1]);
                return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) ;
            })
            .attr("x", function(d, i) { 
                return -(_w_value + w_gap + w_line + _radius);
            })
            .attr("dy", ".35em")
            .attr("text-anchor", "start")
            // .text(function(d) {return d.data.val; })
            ;


        // update lines to initial values               
        _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
             .data(function(d){ return _pie(d.group)})
             // .selectAll("line")
             // .transition().duration(g_duration)
             .attr("x1", function(d,i) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));})
             .attr("y1", function(d) { return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0);})
             .attr("x2", function(d) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));})
             .attr("y2", function(d) { return _arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0);})



        // // update lines according to the _data width               
        _bodyG.selectAll("g.chart-groups")
            .selectAll("line")
             .data(function(d){ return _pie(d.group)})
             // .selectAll("line")
             .transition().duration(g_duration)
             .attr("x2", function(d) { return _arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0)});



        // update dots on lines 
        _bodyG.selectAll("g.chart-groups")
            .selectAll("rect")
            .data(function(d){ return _pie(d.group)})
            .attr("width", dot_size)
            .attr("height", dot_size)
            .attr("transform", function(d) { return "translate(" + (-(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length))) + "," + (_arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) - dot_size/2) + ")"; })
            .transition().duration(g_duration)
            .attr("transform", function(d) { return "translate(" + (_arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0)) + "," + (_arc2.centroid(d)[1] + ((d.data.offsetY)?d.data.offsetY:0) - dot_size/2) + ")"; });




        // update values  
        _bodyG.selectAll("g.chart-groups")
            // .data(_data)
            // .data(function(d){console.log("d.group     ",d.group); return d.group})
            .selectAll("text")
            // .text(function(d){console.log("d.data     ",d.data); return d.data.val;})
            .transition().duration(g_duration)
            .tween("text", function(d) {
               // console.log(d);
                // get current value as starting point for tween animation
               var currentValue = +this.textContent;
               var ii = d3.interpolate(currentValue, d.data.val),
                   prec = (d.data.val + "").split("."),
                   round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;
                // console.log(d3.select(this).attr("class"));
                // d3.select(this).attr("class","chart-value_text");
                // console.log(d3.select(this).attr("class"));

                // this.attr( 'class', 'chart-value_text' );
               return function(t) {
                   // this.textContent = Math.round(ii(t));
                   // this.attr("class","chart-value_text");
                   this.textContent = local_num_format(Math.round(ii(t) * round) / round);
                   // d3.select(this).attr("font-size","10px");

               };
            });


        // update group names  
        tmp_height=0;
        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)
            // .enter()
            // .transition().duration(g_duration/4)
            .attr("transform", function(d, i) { 
                return "translate(" + ( (i)*(_w_value + w_gap + w_line + _radius) + i * ( _radius + gap_between_groups )) + "," + 0 +  ")";
            })
            .call(wrap, _pieWidth, 0, _group_names_TextWidth, _group_names_TextHeight);

        _bodyG.selectAll("text.chart-group_text")
            // .selectAll("text")
            .data(_data)    
            .call(textstyle);

        // // create array to link arc-line-dot-value    
        // _bodyG.selectAll("g.chart-groups")
        //     // .data(_data)
        //     .selectAll("text")
        //     // .data(function(d){ return d.group})
        //     .data(function(d){ return _pie(d.group)})
        //     .call(place_values, 1, 2, 3);

    }  // end of updatePies()


    // Store the displayed angles in _current.
    // Then, interpolate from _current to the new angles.
    // During the transition, _current is updated in-place by d3.interpolate.
    function arcTween(a) {
      var i = d3.interpolate(this._current, a);
      this._current = i(0);
      return function(t) {
        return _arc(i(t));
      };
    }


    function place_values(dataset, AA, BB, CC) {
      // txtWidth.length = 0;

      var vals=[];
      // console.log("place_value:   ",dataset);

      // var test = d3.select(this);
      // console.log("place_value:   ",test);

      // for(var ii = 0; ii < dataset.length; ii++) {
      //   // dataset.each(function(d,i) {
      //       vals[ii] = dataset[ii];
      //       vals[ii].forEach( function(d,i){
      //           // console.log(d.__data__,i,_arc2.centroid(d.__data__));
      //       })
      //       // vals[ii].push(d);
      //   // })
      // };

      dataset.each(function(d,i) {

        console.log("place_value:      ",d,i);

        // d.selectAll("g.chart-bars text")
        //     // .append("text")
        //     .text(function(dd){console.log("place_value:   ",dd,ii); return (dd)});


        vals.push(d);
        // vals.push({
        //     "index_base":i,
        //     "value": d.data.val,
        //     "x_txt": -(_w_value + w_gap + w_line + _radius),
        //     "y_txt": _arc2.centroid(d)[1],
        //     "x_line": _arc2.centroid(d)[0],
        //     "y_txt": _arc2.centroid(d)[1]
        // });



      //   var text = d3.select(this),
      //       // words = text.text().split(/[ \n]/).reverse(),
      //       words = text.text().split(/ /).reverse(),
      //       word,
      //       line = [],
      //       lineNumber = 0,
      //       lineHeight = 1.2, // ems
      //       y = text.attr("y"),
      //       dy = parseFloat(text.attr("dy")),
      //       ln = 0;


      //   var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
      //   var tmpMax = 0;
      //   while (word = words.pop()) {
      //       // console.log("_"+word+"_");
      //     line.push(word);
      //     tspan.text(line.join(" "));
      //     // console.log("_"+word+"_","---------------'",tspan.text(),"'", tspan.text().length);       


      //     if (tspan.node().getComputedTextLength() > width) {
      //       line.pop();
      //       tspan.text(line.join(" "));
      //       line = [word];

      //       // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
      //       // that's why ((tmpMax>0)?++lineNumber:lineNumber)
      //       ln = (tmpMax>0)?++lineNumber:lineNumber;
      //       tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word);
      //     }
      //     // console.log(tspan.node().innerHTML);
      //     tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());

      //   }
      //   txtWidth.push(tmpMax);
      //   // console.log(txtWidth);
      //   //replace <\d+> in data texts to superscript
      //   doSuperscript(text);

      });
    console.log("place_value:   ",vals);

    }



    function renderLegend() {
    // Legend  

        // add legend

        var legendContainer = _bodyG.selectAll("g.chart-legend")    
            .data(_categoryMap)
            // .data(function(d){ return d.group})
            .enter()
            .append("g")
            // .attr("id", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })
            .attr("class", function(d) { return "tag" + uid + d.category.replace(_tag_replace, ''); })
            .classed('chart-legend', true)


        _bodyG.selectAll("g.chart-legend")    
            .data(_categoryMap)
            .attr("visibility","hidden")  // turn legend off to make all calculations for word wrap
            .attr("transform", function(d,i) { return "translate(" +  (i*_legendSpace) + "," + (_height_chart + gap_to_legend) + ")"; })
            .on("click", function(d) {return onClick_Legend(d)});

        legendContainer    
                .append("path")

        legendContainer    
                .append("text")                                    
                .attr("class", "chart-legend_text")


        _bodyG.selectAll("g.chart-legend path")    
            // .data(_categoryMap)
            .attr({
                "fill": function(d) { return _data2colors[d.category]},
                "d": function(d) { return barFun(w_legend_bar)}
            });
            

        _bodyG.selectAll("g.chart-legend text.chart-legend_text")    
            // .data(_categoryMap)
            // .append("text") 
            .attr("x", function(d,i) { return (w_legend_bar + w_gap); }) 
            .attr("y", h_legend_text)
            // .attr("dy", ".35em")
            .attr("dy", "0")
            .text( function(d){ return d.category;})
            // .transition().delay(2000)
            .call(wrap, _legendSpace - w_legend_bar - w_gap - w_gap_between_legends, w_legend_bar + w_gap, _legendTextWidth, _legendTextHeight);
            // .on("click", function(d){
            //     newOpacity = d.active ? 0.5 : 1; 
            //     d3.selectAll("#tag"+d.category.replace(/\s+/g, ''))
            //         .transition().duration(100) 
            //         .style("opacity", newOpacity); 
            //     d.active = !d.active;
            //     });
        var tmp = _legendTextWidth.length;
        console.log("_legendTextWidth :",tmp);
        var w_add = (tmp > 1) ? ((_width - (tmp * (w_legend_bar + w_gap) + d3.sum(_legendTextWidth)) ) /(tmp - 1) - w_gap_between_legends) : 0;
        // w_add is correction of each legend width to fill all _width with legends
        // w_add = 0;
        var w_add2 = _width - d3.sum(_legendTextWidth) - (w_legend_bar + w_gap)*tmp - w_gap_between_legends * (tmp - 1);
        // w_add2 += 2;
        // console.log(w_add2);

        // _legendAlign base value is "left"
        if ( _legendAlign == "right" ) { w_add = 0; }
        if ( _legendAlign == "justify" ) { w_add2 = 0; }
        if ( _legendAlign == "left" ) { w_add = 0; w_add2 = 0; }


        var add_legend_line = 0, lines_max_heights = [];
         lines_max_heights[0] = 0;

        _bodyG.selectAll("g.chart-legend")
            // .transition()
            .attr("transform", 
                function(d,i) {
                    var tmp_w = 0;
                    add_legend_line = 0;
                    for (var ii = 0; ii < i; ii++) {
                        lines_max_heights[add_legend_line] = Math.max(lines_max_heights[add_legend_line], _legendTextHeight[i])
                        tmp_w += w_legend_bar + w_gap +_legendTextWidth[ii] + w_gap_between_legends + w_add;
                        if (tmp_w + w_legend_bar + w_gap +_legendTextWidth[i] + w_gap_between_legends + w_add > _width) {
                            add_legend_line++;
                            tmp_w = 0;
                            lines_max_heights[add_legend_line]=_legendTextHeight[i];
                        }
                    };
                    // if tmp_2 ---> current x-position PLUS total width of current legend element 
                    // IS GREATER than total width
                    // then add new line of legends
                    
                    console.log(_legendTextHeight,_legendTextHeight[i], lines_max_heights);
                    return "translate(" +  (w_add2 + tmp_w) + "," + (_height_chart + gap_to_legend + add_legend_line*(_legendLineHeight * lines_max_heights[add_legend_line] + w_gap_between_legends)) + ")";
                })

        _bodyG.selectAll("g.chart-legend")    
            .attr("visibility","visible"); // turn legend ON. All calculations for word wrap are completed.
        
        // tmp_w = 0;
        // add_legend_line = 0;
        // console.log("---------------------------", _width);
        // for (var ii = 0; ii < _legendTextWidth.length; ii++) {
        //     tmp_w += w_legend_bar + w_gap +_legendTextWidth[ii] + w_gap_between_legends + w_add;
        //     if (tmp_w > _width) add_legend_line++, tmp_w = 0;
        //     console.log(w_add2 + tmp_w,'   :   ', add_legend_line);
        // };

    } // end of renderLegend()


    // function: wrap long lines
    // text - data array
    // width – max width of text
    // xposition – starting X position of text inside <tspan>
    // txtWidth – after function completion this array has width of each of calculated lines

    function wrap(text, width, xposition, txtWidth, txtHeight) {

        var max_legend_height = 0, tmp_legend_height = 0;
        txtWidth.length = 0;
        txtHeight.length = 0;
        text.each(function(d,i) {
            var text = d3.select(this),
                // words = text.text().split(/[ \n]/).reverse(),
                words = text.text().split(/ /).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.2, // ems
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                ln = 0;


            var tspan = text.text(null).append("tspan").attr("x", xposition).attr("y", y).attr("dy", dy + "em");
            var tmpMax = 0;
            while (word = words.pop()) {
                // console.log("_"+word+"_");
              line.push(word);
              tspan.text(line.join(" "));
              // console.log("_"+word+"_","---------------'",tspan.text(),"'", tspan.text().length);       


              if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];

                // if tmpMax == 0 then the very first word in line is LONG. So we don't need to add linenumber.
                // that's why ((tmpMax>0)?++lineNumber:lineNumber)
                ln = (tmpMax>0)?++lineNumber:lineNumber;
                tspan = text.append("tspan").attr("x", xposition).attr("y", y).attr("dy", (ln * lineHeight + dy + "em")).text(word);
              
                tmp_legend_height++;

              }
              // console.log(tspan.node().innerHTML);
              tmpMax =  Math.max(tmpMax,tspan.node().getComputedTextLength());

            }
            txtWidth.push(tmpMax);
            max_legend_height = Math.max(max_legend_height,tmp_legend_height);
            txtHeight.push(max_legend_height);
            // console.log(text.text(), txtHeight);
            // console.log(text.text(), tmp_legend_height, max_legend_height);
            tmp_legend_height = 0;
            //replace <\d+> in data texts to superscript
            doSuperscript(text);

        });
    }

    function doSuperscript(text){
        //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
        // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
        //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
        if (text.html()){
            //replace <\d+> in data texts to superscript
            if(RegExp(/\&lt\;(\d+)\&gt\;/).test(text.html())) {
                var num = text.html().match(/\&lt\;(\d+)\&gt\;/)[1],
                    newtext = text.html().replace(/\&lt\;(\d+)\&gt\;/,'</tspan><tspan class="chart-sup" dy="-3">'+num+'</tspan><tspan>');
                // console.log(newtext);
                text.html(newtext);
            }
        } else {console.log("!!! https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");}
    }

    function textstyle(dd){

        dd.each(function(d,i) {
            var text = d3.select(this);

            //!!!!!!!!!! usually we cant get .html property of SVG text (chrome can)
            // !!!!!!!!!! so we use https://code.google.com/p/innersvg/
            //  see more about d3js selection.html() here  https://github.com/mbostock/d3/wiki/Selections#html
            if (text.html()){
                //replace <*> in data texts to blue / slash
                var newtext = text.html().replace(/\&lt\;\*\&gt\;/,'</tspan><tspan class="chart-blue">/</tspan><tspan>');
                text.html(newtext);
            } else {
                console.log("!!!https://code.google.com/p/innersvg/ is necessary to get access to innerSVG elements");
                    // !!!!!!!!! https://code.google.com/p/innersvg/   needed here everywhere except chrome
            }

        })
    }

    function onClick_Legend(d){

        clearTimeout(_timerId);

        // Determine if current line is visible

        var newOpacity = 1,
            check_active = d.active;

        if (check_active) {
            _categoryMap.forEach( function(dd){ check_active = check_active && dd.active;} )
            newOpacity = check_active ? fadeLevel : 1;

        } else {
            newOpacity = fadeLevel;
            check_active = true;
            d.active = true;

        }

        // Hide or show the elements based on the CLASS

        // set opacity of other to newOpacity
        var cur_category = d.category;
        _bodyG.selectAll("[class^='tag']")
            .filter( function(dd) {
                return (this.id == "tag" + uid + cur_category.replace(_tag_replace, '')) ? false : true
            })
            .transition().duration(100) 
            .style("opacity", newOpacity);

         // set opacity of shadow to newOpacity    
        _bodyG.selectAll(".big_shadow")
            .transition().duration(100) 
            .style("opacity", newOpacity); 

        // set opacity of selected to 1
        _bodyG.selectAll(".tag" + uid + cur_category.replace(_tag_replace, ''))
            .transition().duration(100) 
            .style("opacity", 1);

        // Update whether or not the elements are active
        var cur_active = d.active;
        _categoryMap.forEach( function(dd){ dd.active = !check_active;} )
        d.active = cur_active;
        // console.log(d);

        _timerId = setTimeout(function() {
            _bodyG.selectAll("[class^='tag']")
            .transition().duration(g_duration) 
            .style("opacity", 1);

             // set opacity of shadow to newOpacity    
            _bodyG.selectAll(".big_shadow")
            .transition().duration(g_duration) 
            .style("opacity", 1);

            _categoryMap.forEach( function(dd){ dd.active = true;} )
        }, Time2KeepCategoryActivated);
    }

    function onMouseOver_Bar(d) {

        var zoomContainer = d3.select(d.parentNode.parentNode).selectAll(".chart-groups-zoom"),
            pathContainer = d3.select(d.parentNode),
            cur_cat = pathContainer[0][0].__data__.data.category,
            cur_val = pathContainer[0][0].__data__.data.val,
            isactive = false;

        var selected_class = "."+pathContainer.attr("class").match(/\bnum.{4}\_\d+/g)[0];
        // console.log(zoomContainer);

        var s_path = pathContainer.selectAll("path"),
            s_path_fill = s_path.attr("fill"),
            s_path_darker = ColorLuminance(s_path_fill, -0.5),

            // get path description "d"
            s_path_data = s_path.attr("d"),
            // ttt =  get parts of s_path_data with regex
            ttt = s_path_data.match(/^M(.+)\,(.+)A(.+)\,([^\s]+)\s.+\s(.+)\,(.+)L/),
            side_shadow_path_data_start = "M"+ttt[1]+","+ttt[2]+"l"+0+","+(-0)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z",
            side_shadow_path_data = "M"+ttt[1]+","+ttt[2]+"l"+barZoom+","+(-barZoom)+"l"+(-ttt[1])+","+(-ttt[2])+"L0,0Z";

        // console.log(s_path.attr("d"));

        // d3.selectAll("line"+selected_class)
        // .attr("display","none");

        // zoomContainer
        //     .text("AAAAAAAAAAA")

        onMouseOver_Tolltip(cur_val);


        // parent_pathContainer
        //     .selectAll(".chart-groups")
        //     .append("g")
        //     .attr("class", "AAAAAAAAAAAAAAAAAAAAA")

        _categoryMap.forEach( function(el) {
            if (el.category===cur_cat &&  el.active==true) isactive = true; 
        })

        // console.log(pathContainer, cur_cat, cur_val);
        // if this Legend category ic active (not faded)
        if (isactive) {

            // var selected_class =   "num" + uid + "_" + d.data.num;
            // pathContainer
            d3.selectAll("line"+selected_class)
                // .select("line")
                .transition()
                .attr("x1", function(d) { return _arc2.centroid(d)[0] + ((d.data.offsetX)?d.data.offsetX:0);})
                // .attr("x2", function(d) { return _w_group_name + w_gap + _x(d.val) + w_gap;})
                .transition()
                .attr("x1", function(d,i) { return -(_w_value + w_gap + w_line + _radius - (_w_value + w_value_one_character*(d.data.val + "").length));});

                // .attr("x2", function(d) { return _width - w_gap - (_w_value + w_value_one_character*(d.val + "").length)});
 
            // add shadow path same color as arc
            zoomContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .attr({
                    // "id": "chart-zoombar_shadow",
                    "class": "chart-zoombar_shadow"
                    // "fill": "#0086cd"
                })
                .attr("fill", s_path_fill)
                .attr("d", s_path_data)

            // add side shadow    
            zoomContainer
                .append("path")
                .classed("chart-zoombar_shadow_dark",true)
                .attr("fill", s_path_darker)
                .attr("d", side_shadow_path_data_start)

            // add zoombar same color as arc    
            zoomContainer
                .append("path")
                // .transition().duration(g_duration/4)
                .classed("chart-zoombar",true)
                .attr("fill", s_path_fill)
                .attr("d", s_path_data)

            // color shadow darker    
            zoomContainer
                .select("path.chart-zoombar_shadow")
                .attr("fill", s_path_darker)

            zoomContainer
                .select("path.chart-zoombar")
                .transition().duration(g_duration/4)
                .attr("transform", "translate(" + barZoom + "," + (-barZoom) + ")")

            // update side shadow path with new "d"
            zoomContainer
                .select("path.chart-zoombar_shadow_dark")
                .transition().duration(g_duration/4)
                .attr("d", side_shadow_path_data)



            // make selected value bold    
            d3.selectAll("text"+selected_class)
                // .selectAll("text")
                .classed("chart-selected_value", true);


        }
    }

    function onMouseOut_Bar(d) {


        onMouseOut_Tolltip();

        var zoomContainer = d3.select(d.parentNode.parentNode).selectAll(".chart-groups-zoom"),
            pathContainer = d3.select(d.parentNode);

        var selected_class = "."+pathContainer.attr("class").match(/\bnum.{4}\_\d+/g)[0];

        zoomContainer
            .selectAll(".chart-zoombar")
            // .transition().duration(50)
            .attr("transform", "translate(" + (0) + "," + (0) + ")")
            .remove();
        zoomContainer
            .selectAll(".chart-zoombar_shadow_dark")
            .remove();
        zoomContainer
            .selectAll(".chart-zoombar_shadow")
            .remove();
        d3.selectAll("text"+selected_class)
            // .selectAll("text")
            .classed("chart-selected_value", false);


        // var thisBar = d3.select(d);
        // // console.log(thisBar);

        // thisBar
        // .transition().duration(g_duration/4)
        // .attr({
        //     "d": function(d) { return barFun(_x(d.val))},
        //     "fill": function(d) { return _data2colors[d.category]; }
        // })
    }

    function onMouseMove_Bar(d) {

        onMouseMove_Tolltip();

    }

    function onMouseOver_Tolltip(value) {if (_tooltipOn) return _tooltip.text(value).style("visibility", "visible");}
    function onMouseMove_Tolltip() {if (_tooltipOn) return _tooltip.style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");}
    function onMouseOut_Tolltip() {if (_tooltipOn) return _tooltip.style("visibility", "hidden");}


    
    _chart.setSeries(cdata);
    _chart.render();
    return _chart;
}



// @codekit-append "cmain-append.js";

