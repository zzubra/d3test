/*===============================
=            Globals            =
===============================*/

/**
*
* Chart data 
*
**/

var cdata_colors1 = ["#999999","#21a7d6","#cdcdce","#78bfe2"];


//  \n  первод строки
// <*>  синий слеш для буллетов
// <ЦИФРЫ>  синий суперскрипт
// \xA0  неразрывный
// legend_align: "left" - по умолчанию,"right","justify"


var c01_01_data = {
    title: "Объем кредитного портфеля банка развития, млрд рублей",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 349, "category": "Стратегия"},
            {"val": 349, "category": "Факт"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 454, "category": "Стратегия"},
            {"val": 505, "category": "Факт"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 531, "category": "Стратегия"},
            {"val": 720, "category": "Факт"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 622, "category": "Стратегия"},
            {"val": 975, "category": "Факт"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 727, "category": "Стратегия"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 850, "category": "Стратегия"},
            ]
        }
    ]
};
var en_c01_01_data = {
    title: "Development Bank’s Loan Portfolio, RUB bn",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 349, "category": "Strategy"},
            {"val": 349, "category": "Actual result"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 454, "category": "Strategy"},
            {"val": 505, "category": "Actual result"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 531, "category": "Strategy"},
            {"val": 720, "category": "Actual result"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 622, "category": "Strategy"},
            {"val": 975, "category": "Actual result"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 727, "category": "Strategy"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 850, "category": "Strategy"},
            ]
        }
    ]
};



var c01_03_data = {
    title: "Объем кредитов, предоставляемых в целях реализации инвестиционных проектов, млрд рублей",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 306, "category": "Стратегия"},
            {"val": 306, "category": "Факт"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 416, "category": "Стратегия"},
            {"val": 459, "category": "Факт"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 512, "category": "Стратегия"},
            {"val": 669, "category": "Факт"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 598, "category": "Стратегия"},
            {"val": 893, "category": "Факт"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 700, "category": "Стратегия"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 822, "category": "Стратегия"},
            ]
        }
    ]
};
var en_c01_03_data = {
    title: "Loans Extended for Investment Projects, RUB bn",
    w_group_name: 30,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"2010",
            "group": [
            {"val": 306, "category": "Strategy"},
            {"val": 306, "category": "Actual result"}
            ]
        },

        { "group_name":"2011",
            "group": [
            {"val": 416, "category": "Strategy"},
            {"val": 459, "category": "Actual result"}
            ]
        },

        { "group_name":"2012",
            "group": [
            {"val": 512, "category": "Strategy"},
            {"val": 669, "category": "Actual result"}
            ]
        },

        {"group_name":"2013",
            "group": [
            {"val": 598, "category": "Strategy"},
            {"val": 893, "category": "Actual result"}
            ]
        },

        { "group_name":"2014",
            "group": [
            {"val": 700, "category": "Strategy"},
            ]
        },
        { "group_name":"2015",
            "group": [
            {"val": 822, "category": "Strategy"},
            ]
        }
    ]
};


var c01_04_data = {
    title: "Динамика основных финансовых показателей, млрд рублей",
    w_group_name: 160,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#78bfe2","#21a7d6"],
    data: [
        { "group_name":"Валюта баланса<2>",
            "group": [
            {"val": 2296.5, "category": "01.01.2012"},
            {"val": 2588.9, "category": "01.01.2013"},
            {"val": 2966.3, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Собственные средства\n(капитал)",
            "group": [
            {"val": 336.8, "category": "01.01.2012"},
            {"val": 314.0, "category": "01.01.2013"},
            {"val": 335.4, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Прибыль",
            "group": [
            {"val": 19.79, "category": "01.01.2012"},
            {"val": 5.41, "category": "01.01.2013"},
            {"val": 20.58, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Ресурсы, привлеченные на\xA0срочной основе от\xA0банков",
            "group": [
            {"val": 314.5, "category": "01.01.2012"},
            {"val": 386.5, "category": "01.01.2013"},
            {"val": 443.9, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Ресурсы, привлеченные в\xA0результате размещения облигаций",
            "group": [
            {"val": 174.6, "category": "01.01.2012"},
            {"val": 271.2, "category": "01.01.2013"},
            {"val": 470.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Вложения инвестиционного характера в\xA0акции (доли в\xA0уставном капитале) организаций",
            "group": [
            {"val": 232.6, "category": "01.01.2012"},
            {"val": 294.4, "category": "01.01.2013"},
            {"val": 338.5, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Кредитный портфель Внешэкономбанка, в\xA0том числе:",
            "group": [
            {"val": 766.6, "category": "01.01.2012"},
            {"val": 1002.3, "category": "01.01.2013"},
            {"val": 1318.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0кредитный портфель \xA0\xA0\xA0банка\xA0развития<3>",
            "group": [
            {"val": 505.4, "category": "01.01.2012"},
            {"val": 720.2, "category": "01.01.2013"},
            {"val": 974.6, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0прочие кредиты",
            "group": [
            {"val": 261.2, "category": "01.01.2012"},
            {"val": 282.1, "category": "01.01.2013"},
            {"val": 344.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Портфель ценных бумаг<4>",
            "group": [
            {"val": 539.2, "category": "01.01.2012"},
            {"val": 517.8, "category": "01.01.2013"},
            {"val": 508.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Портфель гарантий и\xA0поручительств",
            "group": [
            {"val": 132.7, "category": "01.01.2012"},
            {"val": 133.0, "category": "01.01.2013"},
            {"val": 219.2, "category": "01.01.2014"}
            ]
        }
    ]
};
var en_c01_04_data = {
    title: "Development Dynamics: Major Financial Highlights, RUB bn",
    w_group_name: 160,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#78bfe2","#21a7d6"],
    data: [
        { "group_name":"Total assets<2>",
            "group": [
            {"val": 2296.5, "category": "01.01.2012"},
            {"val": 2588.9, "category": "01.01.2013"},
            {"val": 2966.3, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Capital",
            "group": [
            {"val": 336.8, "category": "01.01.2012"},
            {"val": 314.0, "category": "01.01.2013"},
            {"val": 335.4, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Income",
            "group": [
            {"val": 19.79, "category": "01.01.2012"},
            {"val": 5.41, "category": "01.01.2013"},
            {"val": 20.58, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Term resources raised from banks",
            "group": [
            {"val": 314.5, "category": "01.01.2012"},
            {"val": 386.5, "category": "01.01.2013"},
            {"val": 443.9, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Resources raised from bond placement",
            "group": [
            {"val": 174.6, "category": "01.01.2012"},
            {"val": 271.2, "category": "01.01.2013"},
            {"val": 470.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Equity investment in authorized capital of\xA0companies",
            "group": [
            {"val": 232.6, "category": "01.01.2012"},
            {"val": 294.4, "category": "01.01.2013"},
            {"val": 338.5, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Loan portfolio of Vnesheconom­bank, including:",
            "group": [
            {"val": 766.6, "category": "01.01.2012"},
            {"val": 1002.3, "category": "01.01.2013"},
            {"val": 1318.7, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0Loan portfolio of the Bank \xA0\xA0\xA0for\xA0Development<3>",
            "group": [
            {"val": 505.4, "category": "01.01.2012"},
            {"val": 720.2, "category": "01.01.2013"},
            {"val": 974.6, "category": "01.01.2014"}
            ]
        },
        { "group_name":"<*>\xA0\xA0Other loans",
            "group": [
            {"val": 261.2, "category": "01.01.2012"},
            {"val": 282.1, "category": "01.01.2013"},
            {"val": 344.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Securities portfolio<4>",
            "group": [
            {"val": 539.2, "category": "01.01.2012"},
            {"val": 517.8, "category": "01.01.2013"},
            {"val": 508.1, "category": "01.01.2014"}
            ]
        },
        { "group_name":"Portfolio of guarantees and sureties",
            "group": [
            {"val": 132.7, "category": "01.01.2012"},
            {"val": 133.0, "category": "01.01.2013"},
            {"val": 219.2, "category": "01.01.2014"}
            ]
        }
    ]
};


var c02_03_data = {
    title: "Портфель «инвестиционных» кредитов (в разрезе основных целей их реализации), млрд рублей",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Развитие инфраструктуры",
            "group": [
            {"val": 385.9, "category": "01.01.2014"},
            {"val": 267.8, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Создание производства",
            "group": [
            {"val": 308.1, "category": "01.01.2014"},
            {"val": 230.0, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Развитие производства",
            "group": [
            {"val": 143.4, "category": "01.01.2014"},
            {"val": 121.9, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Корпоративное финансирование",
            "group": [
            {"val": 38.5, "category": "01.01.2014"},
            {"val": 35.5, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Иные цели",
            "group": [
            {"val": 17.1, "category": "01.01.2014"},
            {"val": 13.7, "category": "01.01.2013"}
            ]
        }

    ]
};
var en_c02_03_data = {
    title: "Portfolio of investment loans (by projects categorized by delivery goals), RUB bn",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Development of infrastructure",
            "group": [
            {"val": 385.9, "category": "01.01.2014"},
            {"val": 267.8, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Construction of production facilities",
            "group": [
            {"val": 308.1, "category": "01.01.2014"},
            {"val": 230.0, "category": "01.01.2013"}
            ]
        },

        { "group_name":"Development of production facilities",
            "group": [
            {"val": 143.4, "category": "01.01.2014"},
            {"val": 121.9, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Corporate finance",
            "group": [
            {"val": 38.5, "category": "01.01.2014"},
            {"val": 35.5, "category": "01.01.2013"}
            ]
        },

        {"group_name":"Other goals",
            "group": [
            {"val": 17.1, "category": "01.01.2014"},
            {"val": 13.7, "category": "01.01.2013"}
            ]
        }

    ]
};


var c02_06_data = {
    title: "Динамика показателей деятельности по поддержке экспорта, млрд рублей",
    w_group_name: 60,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#78bfe2","#999999"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 47.93, "category": "Гарантии в целях поддержки экспорта"},
            {"val": 12.07, "category": "Предэкспортное финансирование"},
            {"val": 2.57, "category": "Экспортные кредиты"}
            ]
        },
        { "group_name":"01.01.2014",
            "group": [
            {"val": 99.57, "category": "Гарантии в целях поддержки экспорта"},
            {"val": 6.88, "category": "Предэкспортное финансирование"},
            {"val": 15.65, "category": "Экспортные кредиты"}
            ]
        }
    ]
};
var en_c02_06_data = {
    title: "Development dynamics: export support activities (RUB, bn)",
    w_group_name: 60,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#78bfe2","#999999"],
    data: [
        { "group_name":"01.01.2013",
            "group": [
            {"val": 47.93, "category": "Guarantees to support exports"},
            {"val": 12.07, "category": "Pre-export finance"},
            {"val": 2.57, "category": "Export credits"}
            ]
        },
        { "group_name":"01.01.2014",
            "group": [
            {"val": 99.57, "category": "Guarantees to support exports"},
            {"val": 6.88, "category": "Pre-export finance"},
            {"val": 15.65, "category": "Export credits"}
            ]
        }
    ]
};


var c02_07_data = {
    title: "Структура средств, привлеченных на рынках капитала, в разрезе срочности, млрд рублей",
    w_group_name: 110,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Свыше 5\xA0лет",
            "group": [
            {"val": 454.3, "category": "01.01.2014"},
            {"val": 400.6, "category": "01.01.2013"}
            ]
        },
        { "group_name":"От 3 до\xA05\xA0лет",
            "group": [
            {"val": 236.4, "category": "01.01.2014"},
            {"val": 24.8, "category": "01.01.2013"}
            ]
        },
        { "group_name":"От 1 года до\xA03\xA0лет",
            "group": [
            {"val": 122.7, "category": "01.01.2014"},
            {"val": 102.4, "category": "01.01.2013"}
            ]
        }
    ]
};
var en_c02_07_data = {
    title: "Structure of funds borrowed in capital markets (by maturity), RUB bn",
    w_group_name: 110,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#21a7d6","#999999"],
    data: [
        { "group_name":"Over 5 years",
            "group": [
            {"val": 454.3, "category": "01.01.2014"},
            {"val": 400.6, "category": "01.01.2013"}
            ]
        },
        { "group_name":"3 to 5 years",
            "group": [
            {"val": 236.4, "category": "01.01.2014"},
            {"val": 24.8, "category": "01.01.2013"}
            ]
        },
        { "group_name":"1 to 3 years",
            "group": [
            {"val": 122.7, "category": "01.01.2014"},
            {"val": 102.4, "category": "01.01.2013"}
            ]
        }
    ]
};


var c03_01_data = {
    title: "Оборот по операциям привлечения/размещения краткосрочных МБК, млрд рублей (эквивалент по среднему курсу)",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"Привлеченные средства",
            "group": [
            {"val": 5491, "category": "2012"},
            {"val": 5056, "category": "2013"}
            ]
        },

        { "group_name":"Размещенные средства",
            "group": [
            {"val": 7385, "category": "2012"},
            {"val": 7576, "category": "2013"}
            ]
        }
    ]
};
var en_c03_01_data = {
    title: "Turnover of money market operations (short-term interbank loans), RUB bn (in equivalent to an average yearly exchange rate)",
    w_group_name: 90,
    legendHeight: 30,
    tooltip : false,
    legend_align: "right",
    colors: ["#999999","#21a7d6"],
    data: [
        { "group_name":"Funds raised",
            "group": [
            {"val": 5491, "category": "2012"},
            {"val": 5056, "category": "2013"}
            ]
        },

        { "group_name":"Funds placed",
            "group": [
            {"val": 7385, "category": "2012"},
            {"val": 7576, "category": "2013"}
            ]
        }
    ]
};

